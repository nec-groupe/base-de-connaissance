---
description: >-
  L'air est plus ou moins saturé en vapeur d'eau (100% d'humidité correspond à
  un air complètement saturé en eau, 0% correspond à pas d'eau du tout dans
  l'air). On peut récupérer cette eau.
---

# Capter l'humidité

Les murets de pierres ou de tuiles permettent de capter et de conserver l'humidité. Ils permettent également de limiter l'érosion.

On peut également récupérer les gouttelettes d'eau présentes dans l'air grace à des filets verticaux.
