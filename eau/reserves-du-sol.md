---
description: .
---

# Réserves du sol

Les réserves d'eau d'un sol sont corréllées à sa [texture](../sol/texture-du-sol.md), à sa [structure](../sol/structure.md) et à sa [porosité](../sol/porosite.md).

Les réserves d'eau du sol sont inégalement réparties dans le temps (au fil des saisons) et dans l'espace (selon les régions).

La réserve utile en eau d'un sol est la partie de l'eau d'un sol qui est disponible pour les plantes. Une partie de l'eau d'un sol est inaccessible aux plantes car elle est trop fortement retenue par le sol, c'est la réserve résiduelle.
