---
description: .
---

# Récupération naturelle

Avec les mécanismes d'évapotranspiration et de circulation de l'air (voir [Forêt](../milieux/foret.md)), et avec le travail des [champignons mycorhiziens](../sol/champignons-mycorhiziens.md), et dans un système bien optimisé, 200mm d'eau passent dans les plantes pour chaque 100mm d'eau de pluie, avec la ré-absorption de l'eau évapotranspirée. D'où un surplus d'eau, qui fait couler des sources par endroits.
