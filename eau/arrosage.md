---
description: .
---

# Arrosage

L'arrosage permet d'augmenter les chances que la culture réussisse, de traverser les sécheresses et d'augmenter le rendement.&#x20;

Il faut apporter la juste dose d'eau au bon moment. La quantité d'eau à apporter en arrosage dépend de plusieurs facteurs ([porosité](../sol/porosite.md), [texture](../sol/texture-du-sol.md) et [structure](../sol/structure.md) du sol, conditions climatiques, microclimats, période du cycle des plantes, etc), il est donc difficile de dire quand arroser et avec quelle quantité d'eau : ni trop (parasites, manque d'air), ni trop peu. Dans tous les cas, l'arrosage n'est qu'un complément d'eau, car les sols couvrent la plupart des besoins en eau des cultures. Pour se passer d'arrosage au maximum, on peut pailler un maximum, et d'avoir des cultures résistantes au manque d'eau, et avec un système racinaire profond.

Il est préférable d'apporter beaucoup d'eau en un arrosage (si possible en deux passages successifs), dans le but de reconstituer les réserves du sol en profondeur et de forcer la plante à s'enraciner profondément. Dans le cas inverse, le risque est que la plante développe un système racinaire superficiel car l'eau ne sera disponible qu'en surface, exposant ainsi la plante à une sécheresse à laquelle elle aurait pu résister autrement. Lorsque la plante est jeune et pas encore bien enracinée, on peut cependant arroser régulièrement et en plus petites quantités.

Également, il est préférable d'arroser les cultures le matin en automne et au printemps, dans le but d'éviter l'association de l'humidité de l'arrosage et du froid de la nuit, qui est préjudiciable aux plantes. En été, il est préférable d'arroser le soir, pour éviter les pertes d'eau par évaporation et évapotranspiration pendant la journée.&#x20;

#### La quantité d'arrosage nécessaire dépend des cultures :

L'ordre de grandeur des besoins en eau d'une parcelle est de 1000mm par an, mais ça dépend de beaucoup de facteurs.

Certaines cultures demandent beaucoup d'arrosage (estimation : 120mm/mois en été), comme les légumes-feuilles, qui ont une grande surface foliaire et sont donc plus sujets à l'évapotranspiration (évaporation d'eau à la surface des feuilles), et qui ont un système racinaire superficiel qui ne permet pas d'aller chercher des réserves d'eau profondes. Les légumes-fruits qui poussent en été pendant les fortes chaleurs demandent aussi beaucoup d'eau : tomates, aubergines, piments, poivrons, etc., ainsi que les légumes volumineux comme les cucurbitacées. Ces cultures doivent être arrosées lorsque la surface de la terre est sèche.&#x20;

Certaines cultures demandent peu d'arrosage, comme les légumes racine : la partie essentielle de la plante se trouve sous terre, à l'abri des grosses déperditions d'eau, et la plante pousse principalement en-dehors de l'été et lorsque les réserves d'eau du sol sont suffisantes. Ces cultures, ainsi que celle qui ne demandent pas d'arrosage, sont plus sensibles aux pourritures et maladies lorsqu'il y a trop d'eau. Ces cultures doivent être arrosées lorsque la terre est sèche sur une profondeur de 8 à 10cm.&#x20;

Certaines cultures ne demandent pas d'arrosage, comme l'ail, les oignons, les échalottes, etc., sauf un peu en cas de canicule.

D'autres cultures ont un besoin moyen en eau et en arrosage : il faut les arroser lorsque la terre est sèche sur 4 à 5cm de profondeur.

Certaines cultures trop arrosées après la fin de la floraison risquent de produire des légumes sensibles aux maladies, difficiles à conserver et peu goûteux.&#x20;

Pour une surface supérieure à 5000m², un puits ne suffit pas. Il faut un étang, une rivière, un lac, un réservoir d'eau ou un marais.

### Pluviomètre

Un pluviomètre est un outil en plastique ou en verre qui permet de mesurer la quantité d'eau de pluie tombée. On peut s'en servir pour adapter son arrosage si on l'observe régulièrement. 1mm d'eau tombée correspond à 1L/m², et il faut environ 5mm pour un bon arrosage, pour donner une idée.&#x20;

### Tuyau microporeux

Utile dans les endroits peu accessibles

### Goutte-à-goutte

Utile dans les endroits peu accessibles, économe. Il faut retirer les tuyaux avant chaque binage.&#x20;

### Oyas

Un oya est un pot en céramique poreuse qu'on remplit d'eau afin qu'elle se disperse dans le sol autour de l'oya, et aux plantes autour (environ 1m² autour pour un oya de 10L). La porosité de la céramique doit être ni trop grande, ni trop petite, et dépend du type d'argile utilisé, du temps de cuisson et de l'épaisseur de l'oya. Il y a deux types d'oyas : à planter (le réservoir est à l'air libre et une partie est dans le sol) et à enterrer (réservoir dans le sol). Le temps entre deux remplissages d'un oya dépend de sa taille et du climat. Pour poser un oya, il faut ameublir, aérer et arroser la terre autour du trou de l'oya, et créer une sorte de boue autour de l'oya. On peut semer ou planter autour de l'oya quelques jours après sa pose. On peut coucher les plantes pour gagner de la place, c'est-à-dire rapprocher les racines de l'oya sous terre. Idéalement, l'oya doit être un élément de conception du jardin.&#x20;
