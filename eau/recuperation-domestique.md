---
description: .
---

# Récupération domestique

On peut récupérer de l'eau de cuisson des légumes, du thé ou du vin, à conditions qu'ils soient dilués, froids et sans sel. L'urine peut aussi être récupérée, diluée 10 fois, à conditions de ne pas contenir de médicaments.&#x20;
