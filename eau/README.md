---
description: >-
  L'eau est nécessaire aux plantes, il est donc vital de savoir la gérer selon
  nos besoins et ceux des plantes.
---

# 💦 Eau



Les végétaux sont composés à près de 80% d'eau. Dans la plante, c'est le moyen de transport de toutes les substances nécessaires aux différentes étape de sa vie : germination, croissance, floraison, production de graines, de fruits, etc.&#x20;
