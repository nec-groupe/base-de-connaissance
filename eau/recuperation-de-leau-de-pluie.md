---
description: .
---

# Récupération de l'eau de pluie

Il tombe entre 500 et 1200mm d'eau par an en France métropolitaine, soit 500 à 1200L par m², selon les régions et les années. On peut récupérer cette eau grâce aux toitures et aux gouttières, ou grace à des baches. La chaine de pluie peut permettre de transporter l'eau verticalement, comme un cheneau (gouttière verticale).&#x20;

