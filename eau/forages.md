---
description: .
---

# Forages, puits, pompes

pompe bélier

Les pompes poussent l'eau et ne la tirent pas, il faut donc la placer le plus proche possible de la réserve d'eau. Filtre à sédiment sur la pompe pour éviter qu'elle se bouche.&#x20;
