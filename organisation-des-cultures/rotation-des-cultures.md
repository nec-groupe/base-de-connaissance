---
description: .
---

# ♻ Rotation des cultures

### Théorie :&#x20;

La rotation des cultures et une technique qui consiste à varier les plantes qu'on fait pousser, année après année, sur un même endroit.

Dans le passé, cette technique a été beaucoup utilisée. Des petites parcelles, exploitées en rotation, apportait beaucoup de diversité aux cultures, ce qui permet une plus grande résilience. Si les patates ne poussent pas ou sont mangées une année, il reste beacoup d'autres cultures, et on va faire pousser autre chose sur cette parcelle l'année d'après pour éviter les mêmes problèmes.

Le but est d'éviter l'apauvrissement des sols et d'éviter d'attirer les ravageurs. Si on laisse des patates sur une même planche plusieurs années de suite par exemple, les nutriments pris dans le sol seront les mêmes, à la même profondeur, et les ravageurs de la patate vont se plaire à cet endoit et détruire la récolte. Au contraire, si on varie les cultures à cet endroit (type de plante : feuille, racine, fleur, fruit, graine, bulbe. Ou famille : solanacées, brassicacées, liliacées, cucrbitacées, etc), les différentes cultures successives vont se nourrir les unes les autres (par exemple, les fruits et feuilles sont plutôt gourmands en nutriments, contrairement aux bulbes, les légumineuses fixent l'azote de l'air dans le sol), piocher des nutriments différents (ceux dont elles ont spécifiquement besoin), à différentes profondeurs, et les ravageurs vont moins avoir le temps de s'installer car leur cycle naturel sera rompu.

#### Mise en place

Le réel bénéfice de la rotation des culture se fait sentir à long terme, sur minimum plusieurs années. Si l'objectif est à court terme, alors il ne faut pas trop investir dans une rotation : certaines cultures seront probablement ajoutées ou abandonnées en route, et l'espace accordée à certaines pourra changer. Il faut étudier différents systèmes de rotation, par des livres ou des producteurs par exemple, pour en saisir pleinement la logique. De plus, il faut évidemment prendre en compte les particularités du [site](../site.md) pour établir la rotation (humidité, soleil, irrigation, types de sols, etc). Il faut aussi ajuster les plans de rotation et de production en fonction des besoins et de ce qui se passe sur le terrain.&#x20;

La rotation apporte des contraintes, mais ses avantages sont immenses sur le long terme. Le plan, un fois fixé, est pérenne et facile à respecter.

### Pratique :&#x20;

En théorie, la théorie et la pratique c'est la même chose. En pratique, c'est différent.



Notre tableau de rotation des cultures :&#x20;

{% embed url="https://docs.google.com/spreadsheets/d/1y4bcKQfjEx17NvNsQRlKF8RUItLtT-V4AKn51YqZcq0/edit#gid=1168107428" %}
