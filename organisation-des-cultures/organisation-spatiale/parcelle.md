---
description: .
---

# Parcelle

C'est la plus grande unité spatiale. Une parcelle est un espace dans lequel on va délimiter nos jardins. L'idée est d'avoir dans chaque parcelle **autant de jardins que de d'années dans le cycle** de la [rotation-des-cultures.md](../rotation-des-cultures.md "mention") choisie. \
\
Dans notre cas, chaque parcelle aura quatre jardins de surface équivalente.\
\
La taille des parcelles est déterminée en fonction de l'espace total et du nombre de bras disponibles. Compte \~**1000 m²** pour **20 personnes**.
