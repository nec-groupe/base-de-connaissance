---
description: .
---

# 📐 Organisation spatiale

<figure><img src="../../.gitbook/assets/parcelle A.png" alt=""><figcaption></figcaption></figure>

Nous utilisons trois unités spatiales pour désigner nos espaces, listées ici de la plus grande à la plus petite :&#x20;

* [parcelle.md](parcelle.md "mention")
* [jardin.md](jardin.md "mention")
* [planche.md](planche.md "mention")

### Principes généraux&#x20;

Pour organiser efficacement un espace de culture, il faut prendre en compte nos besoins, la fonction de chaque élément (arroser, stocker, se déplacer, etc), ainsi que les besoins de chaque élément (en eau, en lumière, en accessiblité, etc). L'idée est de simplifier le plus possible l'organisation des cultures, les déplacements quotidiens, l'entretien, la compréhension qu'on a de l'espace, etc.&#x20;

Il peut être pertinent de positionner les plantes pérennes (aromatiques ligneuses et vivaces, arbustes fruitiers, etc.) en premier sur le plan, car elles seront là pour plusieurs années. Il faut aussi anticiper la taille définitive des plantes (y compris les engrais verts, les plantes compagnons, etc), pour prendre en compte leurs ombres portées, mais aussi parce que certaines peuvent servir d'abris aux plus basses.&#x20;

Dans tous les cas, il est important de détailler les plans et de vérifier sur place les mesures et la théorie.

À Mauves, on avait besoin d'avoir 4 jardins de même surface, le plus identiques possible et organisés de façon la plus simple possible en fonction de la taille et de la forme de la parcelle. Il nous fallait aussi pouvoir nous déplacer entre les jardins, et prendre en compte l'ail, les framboisiers, fraisiers et pois, qui restent sur place. Sans oublier des jardins expérimentaux, qui peuvent être plus informels (triangles entre les jardins, en bas du schéma, ainsi qu'autour des framboisiers.) La solution qu'on a trouvée répond à tous ces besoins et ne pose pas de problème particulier.

### Standardisation spatiale

L'idée de la standardisation spatiale est de faciliter les calculs, les planifications, et d'uniformiser certains équipements et outils. Les jardins font tous la même surface pour équilibrer les récoltes année après année, et pour faciliter la planification. Les planches font toutes la même longueur pour faciliter la planification encore une fois, mais aussi pour uniformiser des éventuels équipements comme les bâches, les couvertures flottantes, etc, et les outils. Cela sert aussi à faciliter la mesure des rendements, par exemple.&#x20;
