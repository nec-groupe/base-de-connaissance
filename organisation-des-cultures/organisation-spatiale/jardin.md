---
description: .
---

# Jardin

Un jardin est une portion du champ, contenant un certain nombre de planches.\
Le jardin sert à faciliter la gestion de la rotation des cultures ! Chaque jardin a la même surface pour faciliter la planification et équilibrer les récoltes année après année.&#x20;



