---
description: .
---

# 🌾 Organisation des cultures

En 2022, nous avons testé plus de **50 variétés de plantes !** \
\
Les premières années de culture d'un champ ne font traditionnellement pas appel à un plan de rotation des cultures. En effet, ce dernier est difficile à établir et demande de connaitre un peu son champ.&#x20;

## Pourquoi effectuer une rotation des cultures ?

Chaque plante est particulière et à ses propres besoins. Chaque plante va donc **puiser différentes ressources** dans le sol et en apporter d'autres.&#x20;

Dans la nature, les espèces de plantes se mélangent et forment un équilibre entre les besoins et apports, permettant la pousse durable des végétaux.

Dans un potager, sur un champ, les humain·es ont tendance à **concentrer une même espèce** au même endroit. Mécaniquement, si l'on cultive année après année aux mêmes endroits, **le sol va s'appauvrir** en ressources puisées par les plantes, et se charger en ressources apportées par ces plantes. Ainsi, en quelques années, les plantes ne donneront plus autant.

Il existe plusieurs solutions pour y remédier :&#x20;

* Compenser les pertes du sol par des intrants, souvent chimiques :skull\_crossbones: pas de commentaire
* Panacher les plantes pour recréer un équilibre, solution élégante, mais peu pratique
* Changer chaque année l'emplacement des cultures, autrement dit faire une **rotation des cultures**

**TL;DR** Si tu cultives toujours tes plantes au même endroit, tu appauvris le sol, il faut donc **changer les espèces de plantes de place tous les ans**



## Comment effectuer une rotation des cultures ?

[rotation-des-cultures.md](rotation-des-cultures.md "mention")

[organisation-spatiale](organisation-spatiale/ "mention")



