---
description: Azote, potassium, phosphore, souffre, etc.
---

# Éléments minéraux

_**L'azote**_ (N) est principalement produit par la minéralisation de la MO. Il favorise surtout le développement des feuilles. En-dessous de 10°C dans le sol, l'activité biologique du sol devient très faible, il faut donc le nourrir en azote, si possible avec de l'engrais soluble (fumier par exemple). Il ne faut pas non plus surcharger en azote à la fin de la saison, car alors il se transforme en nitrate et rend les légumes toxiques.&#x20;

_**Le phosphore**_ (P) est principalement produit par la minéralisation de la MO, comme l'azote. Il est apporté par le fumier et le compost. L'usage abusif de phosphore est dangereux. Les engrais verts permettent d'apporter de l'azote sans phosphore.

_**Le potassium**_ (K), contrairement à l'azote et au phosphore, ne provient pas de la minéralisation de la MO. Il se trouve sous forme d'argile. Il est facilement disponible aux plantes, mais également facilement lessivable, d'où l'importance du [paillage](../methodes/paillage.md), des [engrais verts](../engrais-verts/), des [haies](../plantes/haies.md), etc. Il peut être utile de fertiliser au sulfate de potassium + compost + fumier dans des sols sablonneux (dont les éléments minéraux sont rapidement lessivés naturellement) ou dans des serres, mais pour la plupart des sols, ce n'est pas nécessaire.

Quelques autres éléments minéraux sont nécessaires à la croissance des plantes. _**Le calcium**_ et _**le souffre**_ se trouvent en quantité suffisante dans les sols, en général. Certains sols sableux peuvent avoir une carence en _**magnésium**_.

Des oligo-éléments, comme le bore ou le mobyldène, peuvent manquer à certaines cultures exigentes.
