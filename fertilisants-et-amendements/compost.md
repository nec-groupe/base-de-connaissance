---
description: .
---

# Compost

Voir "[Le petit guide du compost](../bibliotheque/le-petit-guide-du-compost.md)" dans la bibliothèque.

Le compost est un fertilisant et un amendement produit par la décomposition des matières organiques. C'est un amendement idéal. Il libère l'azote et les éléments fertilisants tout au long de la saison, voire sur plusieurs années. Il détruit les graines de mauvaises herbes et les agents pathogènes.

Quand le compost est encore chaud, il contient déjà une vie très active et peut réchauffer le sol printanier un peu froid. Quand on le met sur le champ, il faut le mélanger à la terre sur quelques cm d'épaisseur pour éviter que l'azote se volatilise.

Vérification de l'humidité du compost : Prendre une poignée de compost dans la main et presser fortement. Un peu d'eau doit s'en échapper. Si trop d'eau s'écoule, rajouter des matières sèches et absorbantes comme des feuilles mortes. S'il est trop sec, arroser avec de l'eau de pluie.

Aération : essentiel, puisque le compostage est un processus aérobie. Espace entre les planches, vers de terre, retournements, bonne humidité, mélange de matières sèches et humides.&#x20;

Tamis : grille de 2cm x 2cm.

### Emplacement

Facile d'accès : proche du champ et de la cuisine, possibilité de passer facilement avec une brouette, pas trop de vent pour éviter le déssèchement des matières, sol drainant et zone non-inondable pour éviter le pourrissement.&#x20;

