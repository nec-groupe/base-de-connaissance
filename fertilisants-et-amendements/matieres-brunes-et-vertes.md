---
description: >-
  Les matières brunes sont source de carbone, et ont un rapport C/N haut
  (supérieur à 15 ou 20), et les matières vertes sont source d'azote et ont un
  rapport C/N bas.
---

# Matières brunes et vertes

C désigne les atomes de carbone, et N les atomes d'azote. Selon les matières organiques, le rapport C/N change, et il faut dans l'idéal arriver à un équilibre de C/N=20-30 environ. Ce rapport est compris entre 1 et 500. L'herbe est à 6 environ, la paille à 150, les branches entre 300 et 500.

Rappellons qu'un paillage sert aussi à nourrir et à structurer le sol, il peut donc être un fertilisant/amendement. À ce titre, et comme pour les autres fertilisants et amendements, c'est une bonne idée de penser à quel type de paillage on met, pour combien de temps, penser à la santé du sol, etc. Il faut équilibrer le rapport C/N, donc équilibrer au maximum les matières vertes et brunes.

### Matières brunes :

(ou matières sèches)

feuilles mortes, branchages, brindilles, troncs, paille, papier, carton, tiges dures, sciure, écorce, cendres.  (Attention aux conifères qui acidifient le sol)

### Matières vertes :&#x20;

(ou matières fraiches)

épluchures, fruits, légumes, fleurs, herbe, mauvaises herbes, feuilles vertes, marc de café.

### Faim d'azote

Lorsqu'on met de la paille (qui contient beaucoup de carbone par rapport à l'azote) dans le sol, les bactéres vont utiliser de l'azote du sol pour la décomposer, ce qui va manquer aux cultures, même si cet azote va être redistribué à la mort des bactéries. C'est ce qu'on appelle la faim d'azote. Il faut alors rajouter des matières riches en azote, comme le fumier ou de l'herbe fauchée, ou alors du purin d'orties.&#x20;

