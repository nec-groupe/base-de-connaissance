---
description: >-
  Un fertilisant nourrit le sol et les plantes, en leur apportant des
  nutriments, des sels minéraux, etc. Un amendement a pour but d'améliorer la
  structure ou la texture du sol, ses propriétés physiques
---

# 🍁 Fertilisants et amendements

Entretenir la fertilité de la terre est nécessaire à la bonne croissance des plantes. La fertilité des sols est très liée à la biodiversité.

Fertilisants organiques : apport de matière organique qui nourrit l'activité biologique du sol.&#x20;

Les fertilisants comme le compost doivent être utilisés avec parcimonie, car ils sont en quantités limités. Ils doivent venir en complément pour ajuster une culture riche en nutriments de base : [rotation des cultures](../organisation-des-cultures/rotation-des-cultures.md), [engrais verts](../engrais-verts/), etc. De plus, une fertilisation abusive est aussi dommageable qu'un fertilisation déficiente.&#x20;

Pour comprendre la fertilité (et donc l'apport de fertilisants à apporter) au sol, on peut se servir d'expertises agronomiques ou de tests existants, en plus d'observer et de comprendre la fertilité de son sol par l'observation des légumes et des plantes.&#x20;

Éléments d'une bonne stratégie de fertilisation selon [Le jardinier-maraîcher](../bibliotheque/le-jardinier-maraicher.md) : analyse du sol en labo (pour le PH, la matière organique, l'humidité, la chaleur), recommendation et interprétation par un agronome en bio pour corriger les carences et déséquilibres du sol en nutriments et en [pH](../sol/ph-du-sol.md), [rotation des cultures](../organisation-des-cultures/rotation-des-cultures.md) et [engrais verts](../engrais-verts/), dosage des fertilisants en fonction du sol et des besoins de chaque légume, **suivi et évolution dans le temps**.

Exemples d'amendements : tourbe, chaux (désacidifie le sol), sable ou argile (pour la [texture](../sol/texture-du-sol.md) du sol)
