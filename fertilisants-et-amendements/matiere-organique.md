---
description: .
---

# Matière organique

(ici : MO)

Lorsque la MO est minéralisée par l'activité biologique (voir [Vie du sol](../sol/vie-du-sol.md)), elle libère de l'azote, du phosphore, du souffre et plusieurs oligo-éléments qui fournissent la nourriture aux plantes (minéralisation). Le reste de l'humus structure le sol (complexe argilo-humique). La MO est donc à la fois le carburant et l'habitat des organismes du sol : elle est donc indispensable.&#x20;

Si on part d'un sol pauvre, il faut commencer par le bâtir : il faut faire un apport initial de MO (mousse de tourbe par exemple), puis maintenir la fertilité du sol avec du compost, des engrais verts, etc.&#x20;

Attention, un taux de MO trop élevé dans un sol peut être le résultat d'une activité biologique insuffisante dûe à un sol acide ou mal drainé par exemple, qui ferait s'accumuler la MO sans qu'elle puisse être minéralisée.&#x20;
