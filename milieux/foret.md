---
description: .
---

# Forêt

On ne va pas définir ce qu'est une forêt ici, mais il faut simplement garder en tête qu'une forêt est un milieu naturel et diversifié : les monocultures d'arbres bien rangés et largement contrôlées par l'homme, et conséquemment avec très peu de biodiversité, n'en font pas partie, car les points communs entre ces deux milieux sont assez peu nombreux.

Les vieux arbres sont mieux préparés aux ravageurs et aux maladies que les plus jeunes, grâce à leur microbiome qui a pu s'adapter et se renforcer au cours du temps. Par conséquent, il peut être pertinent de les garder, pour qu'ils inoculent ce microbiome aux arbres les plus jeunes, qui en ont besoin pour réussir.&#x20;

Importance des lisières (à creuser)

La forêt attire et conserve l'eau. La condensation de l'eau sur les feuilles créent une dépression de l'air (car l'eau sous forme gazeuse se transforme en eau liquide, donc elle ne fait plus partie de l'air, et laisse la place à plus d'air. La pression baisse localement.). Cette dépression de l'air crée des mouvements d'air, qui deviennent des vents.&#x20;

La photosynthèse et l'évapotranspiration (évaporation de l'eau à la surface des feuilles) refroidissent les végétaux (ce sont des processus endothermiques, c'est-à-dire que ces phénomènes physiques absorbent de la chaleur). Ces deux phénomènes créent une dépression sous l'arbre, qui attire l'air chaud et le fait circuler : par conséquent, cela attire beaucoup d'air chargé en humidité, donc beaucoup d'eau. C'est donc pour cela que les couches de végétation sont importantes en forêt et en culture : chaque couche capte l'eau de la couche au-dessus.&#x20;

On peut adapter à l'agriculture 3 axes du fonctionnement des forêts : couverture maximale des sols (par les matières organiques animales et végétales qui viennent de la forêt), plantation d'arbres, diversification des végétaux. En s'aidant de la forêt pour faire de l'agriculture, on peut faire une [forêt-jardin](../techniques-de-cultures/foret-jardin.md).

Les terres agricoles doivent leur richesse en carbone aux forêts qu'il y avait avant, car les forêts stockent du carbone en grande quantité (les terres agricoles aussi, mais beaucoup moins).

La déforestation est délétère et dangereuse. Si on enlève les arbres, on enlève les apports de matière organique, la température de surface du sol monte, il y a plus d'eau au sol, ce qui provoque l'érosion des sols et des éléments nutritifs, la modification des comunautés bactériennes du sol, etc.&#x20;

&#x20;Une forêt, comme une prairie, produit environ 20 tonnes de matière organique sèche (c'est-à-dire sans compter l'eau dans cette matière organique) par hectare et par an (2kg par m²), sans compter la production de bois stockée dans les troncs.&#x20;
