---
description: >-
  Les microclimats sont des variations de climat (température, humidité,
  ensoleillement, etc.) à l'échelle d'une parcelle ou d'un jardin.
---

# Microclimats

Repérer l'exposition au soleil et à l'ombre (arbre, bâti), la pente du terrain, les passages d'air froids, les zones plus chaudes ou qui se réchauffent plus vite (mur noir ou tas de pierres par exemple, qui emmagasinent aussi la chaleur du jour pour la restituer en soirée et la nuit), etc.&#x20;

On peut ensuite utiliser ces connaissances pour intaller des plantes à des endroits du jardin qui leur sont idéaux. Par exemple, des plantes frileuses ou grimpantes sur des façades de bâtiments ensoleillées.
