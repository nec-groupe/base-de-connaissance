---
description: .
---

# Mare

Une mare est une réserve de biodiversité, elle peut donc être utile aux cultures.&#x20;

Un jardin d'eau est différent d'une mare dans le sens ou il est plus ambitieux, plus travaillé : il est plus difficile à mettre en place, les plantes sont choisies pour leurs fonctions, on peut y installer des apliers sous l'eau pour différents types de plantes qui s'y plaisent, une pompe et un régulateur de niveau d'eau, etc.

### Fonctions

Une mare a une eau riche en nutriments, qui peut être utilisée en arrosage pour nourrir les cultures. Le fond de la mare est très riche également, et peut être utilisé en paillage fertile. La mare réverbère aussi les rayons du soleil pour les plantes autour. Elle permet aux animaux autour de boire.

### Mise en place

Objectifs lors de la conception d'une mare : capter, stocker, filtrer et distribuer l'eau.&#x20;

Terrasses immergées pour des végétaux aquatiques comestibles.&#x20;

Gambusies : poissons mangeurs de moustiques qu'on peut installer dans une mare.&#x20;



Le filtrage de l'eau peut se faire par phytoépuration par des roseaux, qui dépolluent les eaux polluées noires (excréments) et grises (domestiques), et apporte un paillage de roseaux très fertile.&#x20;
