---
description: .
---

# Air

Un air chaud et humide est impossible à supporter dans la durée pour l'être humain, car la transpiration est impossible : l'évaporation de l'eau, qui refroidit le corps, est impossible car l'air est déjà chargé en humidité. C'est donc un danger écologique important.
