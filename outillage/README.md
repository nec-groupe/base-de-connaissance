---
description: Cette section présente les outils, leur utilisation et leur entretien !
---

# ⚒ Outillage

Présentation des outils communs, mode d'emploi et d'entretien.

Préparation des planches :&#x20;

* [houe.md](houe.md "mention")
* [grelinette.md](grelinette.md "mention")
* [griffe-piocheuse-3-dents.md](griffe-piocheuse-3-dents.md "mention")
* [rateau.md](rateau.md "mention")

Entretien des outils :&#x20;

* [lime-plate-polyvalente.md](lime-plate-polyvalente.md "mention")
* [pierre-naturelle-aiguise-tout.md](pierre-naturelle-aiguise-tout.md "mention")

À Commun Champ, on a décidé pour plusieurs raisons de ne pas mécaniser et de travailler à la main seulement. Des outils et des véhicules lourds, comme les tracteurs ou les motoculteurs, tassent la terre avec le poids des engins et les vibrations des moteurs, ce qui est mauvais pour la vie du sol. Le labour tue également le [mycélium](https://fr.wikipedia.org/wiki/Myc%C3%A9lium) des champignons et les vers de terre, et toutes sortes d'autres animaux du sol.&#x20;

De plus, le labour avec un tracteur amène un autre problème : la semelle de labour, qui se forme à la limite entre la terre labourée régulièrement jusqu'à une certaine profondeur, et la terre, plus profonde, qui n'est jamais labourée, et qui se tasse. Ces deux milieux deviennent de plus en plus différents aux cours du temps, et finissent par être quasiment hermétiquent l'un par rapport à l'autre. On se prive alors de tout ce que le sol peut nous apporter en profondeur, et on le coupe en deux, pour ainsi dire.&#x20;

Les outils mécanisés ne sont pas forcément plus rentables, surtout dans notre cas, car ils coutent plus cher que des outils non-mécanisés qui restent sophistiqués et efficaces.&#x20;

Travailler à la main est plus pertinent et plus adapté dans une démarche d'autonomie et de travail minimal du sol sur une petite surface, ce qui est notre démarche. Avec moins de labour et de perturbations, la vie du sol se porte mieux, le sol et la vie du sol sont plus stables, et il y a moins d'érosion parce que la [structure du sol](../sol/structure.md) est conservée.

De plus, laisser les gens travailler eux-mêmes la terre est plus gratifiant et meilleur pour la santé : il n'y a pas de bruit, on fait du sport, on prend l'air, etc. Les outils s'ajustent à notre pratique au lieu de la définir.



