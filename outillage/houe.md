---
description: .
---

# Houe

<figure><img src="../.gitbook/assets/houe.jpg" alt=""><figcaption><p>La tête d'une houe pour le travail superficiel.</p></figcaption></figure>

La houe est l'un des premiers outils utilisés par les humain·es. Sa géométrie varie selon l'utilisation qui en est faite. \
\
De notre côté, nous l'utilisons pour **enlever la couche superficielle de végétation et de terre de \~3 cm**, en vue d'une **remise en culture** d'un espace non cultivé précédemment.\
\
La houe permet également de creuser les **passe pieds.**

## Mode d'emploi

L'idée est de lever la houe, et de la laisser tomber et utiliser son poids pour la planter dans le sol.

1. **Lever** la houe
2. La laisser tomber pour **couper verticalement la surface du sol**
3. Tirer la houe pour **décoller la terre**
4. Répéter

{% embed url="https://www.youtube.com/watch?v=in6295CLeWw" %}

## Entretien

La houe **doit être aiguisée**, elle est généralement composée d'un acier meuble pour encaisser les chocs avec les inévitables pierres du terrain. \
\
Il est possible de l'aiguiser avec une [pierre-naturelle-aiguise-tout.md](pierre-naturelle-aiguise-tout.md "mention") ou une [lime-plate-polyvalente.md](lime-plate-polyvalente.md "mention").

