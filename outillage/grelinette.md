---
description: .
---

# Grelinette

<figure><img src="../.gitbook/assets/grelinette (1).jpg" alt=""><figcaption></figcaption></figure>

La grelinette permet de **décompacter et d'aérer le sol** en profondeur, sans toutefois retourner la terre. Cela permet de ne pas modifier la structure du sol, et de **préserver la vie du sol**.\
\
Nous l'utilisons lors de la **préparation des planches de cultures** en hiver.

## Mode d'emploi

La grelinette s'utilise en quelques étapes :

1. **Enfoncer les dents dans le sol**. Pour cela, il faut appuyer avec son pied sur la barre centrale tout en tenant les deux manches
2. Pousser puis tirer les manches alternativement quelques fois pour **aérer le sol**
3. Tirer les manches vers soi pour **soulever la terre**
4. Une fois les manches presque à l'horizontal, soulever alternativement les manches droits et gauches afin de **briser les mottes**
5. Retirer la grelinette du sol et recommencer le long de la planche **5 cm plus loin**.

{% embed url="https://www.youtube.com/watch?v=xrnkbeIH7Rg" %}

## Entretien

A priori rien de particulier à faire, à part la nettoyer et faire attention à ce que les manches soient bien fixés au métal.
