---
description: Une butte est une surélévation d'une planche de culture, de forme arrondie.
---

# Butte

### Application :&#x20;

Les buttes sont particulièrement adaptées pour les terres lourdes, car la surélévation permet de drainer le sol (permet l'évacuation de l'eau) et d'éviter certains problèmes sanitaires comme le pourrissement des racines. Elles apportent aussi un avantage certain pour des régions où les terres sont très pauvres, érodées, dans des zones chaudes ou semi-désertiques par exemple.

### Conception :&#x20;

Placer la butte dans l'axe nord-sud pour que les deux côtés reçoivent la lumière du soleil. Il faut, comme n'importe quelle autre planche de culture, tenir compte des circulations, ombres portées, micro-climats, pentes, etc, de la parcelle, pour concevoir intelligemment le plan des buttes. Voir  [Organisation spatiale](../organisation-des-cultures/organisation-spatiale/).

La butte est un réservoir nutritif, il s'épuise donc en profondeur à mesure que des plantes poussent dessus, et la butte se tasse, même si on apporte régulièrement de la matière organique en surface. Il faut donc prendre en compte la durée de vie de la butte en fonction de la quantité de matière organique présente en profondeur, surtout des grosses branches, troncs, souches etc., qui prennent beaucoup de temps à se décomposer.

### Mise en place :&#x20;

Une butte demande un peu d'énergie et de travail. Élever une petite butte de terre de 1,20m de large et 50cm de haut environ, séparée d'autres buttes ou d'autres cultures de 60 à 80cm.&#x20;

#### Butte simple

Reprendre les 4 premières étapes de la préparation de la [planche permanente](preparation-dune-planche.md).

Puis entasser des branches, troncs, copeaux etc. sur la planche.

Recouvrir ces matières organiques par de la terre fertile (des passe-pieds, ou de la terre autour des jardins par exemple) sur 30 à 50cm, sans tasser, et ajouter des engrais organiques, comme du compost.&#x20;

Égaliser et aplanir la butte, avec des pentes courbes sur les côtés.

Recouvrir la butte de 20cm de paille.

#### Butte autofertile / Hügelkultur

Ce type de butte permet d'entasser plus de matière organique à décomposition lente (grosses branches, troncs etc), elle a donc potentiellement plus de durée de vie.

Creuser sur toute la surface un sillion à une profondeur de 30cm ou plus.

Déposer dans ce sillon les grosses branches, troncs, etc, et combler les espaces vides avec des feuilles, des copeaux, de la sciure, de la terre, etc. Arroser et faire un second apport pour éviter les poches d'air.

Apporter une couche de matière facilement décomposable, comme des feuilles mortes.&#x20;

Ajouter la terre du sillion sur une hauteur de 20cm, puis une couche de copeaux ou de sciure, puis un [paillage](paillage.md).

#### Culture en cuvette

Entre deux buttes, sur les passe-pieds, on peut creuser des cuvettes de 30cm de profondeur environ, qu'on tapisse de galets avant de les remplir de terre. Les pierres emmagasinent la chaleur de la journée et la restituent la nuit. Cela permet de cultiver des légumes tôt dans la saison, ou dans des régions froides, grâce au microclimat créé par les galets et les 2 buttes autour de la cuvette. Par ailleurs, les pierres captent l'humidité ambiante en été pour la condenser.&#x20;

### Culture :&#x20;

Une fois en place, la butte est prête et ne demande plus de maintenance ni de travail du sol. On va cultiver dans cette butte surélevée, idéalement avec un paillis dessus. Dûe aux formes des buttes, la surface cultivable est augmentée de 10 à 15% par rapport à des planches normales. &#x20;

