---
description: >-
  (C'est un type de culture qu'on n'a pas choisit de faire à Mauves, mais ça
  existe)
---

# Lasagne

### Application :&#x20;

La culture en lasagnes ne requiert pas de sol. C'est donc une technique idéale pour des terres abandonnées, des sols pauvres voir inexistants, sur un balcon d'appartement par exemple.&#x20;

Au cours de la culture, la lasagne va se décomposer et se tasser. Le carton du fond se délite et le travail des organismes enrichit le sol sur lequel est posé la lasagne. Le sol ne fera que s'améliorer. Au bout de quelques années, en fonction de l'état de la terre en-dessous, on peut revenir à une culture en pleine terre.

### Conception :&#x20;

Les cultures en lasagnes nécessitent de la matière organique, c'est donc un bon moyen pour se débarrasser des déchets organiques en grosse quantité.&#x20;

Ce type de culture n'a pas beaucoup de longévité, car les cultures vont relativement rapidement manger toutes les matières organiques présentes dans la butte.&#x20;

Dû à la nature éphémère de la lasagne, les plantes vivaces n'ont pas leur place. Priorisez les légumes et les fleurs, qui sont annuels. Priorisez également les plants aux semis, car les semis ont du mal à lever sur les lasagnes.

### Mise en place :

Empiler de bas en haut :&#x20;

Herbe fauchée (éventuellement de la parcelle),&#x20;

Carton (sans encre toxique, ni plastique, ni vernis),&#x20;

Une alternance de [matières brunes et vertes](../fertilisants-et-amendements/matieres-brunes-et-vertes.md) : 4 à 6 couches de 8cm d'épaisseur environ.

compost (ou terre) sur une dizaine de cm d'épaisseur au moins.&#x20;

paillage.

Il faut faire attention à bien donner une forme plane à la lasagne, voire légèrement creuse à l'intérieur, mais pas en dôme.

### Culture :&#x20;

Arroser avant de planter. L'eau doit rentrer en quantité et en profondeur. Comme la lasagne est un milieu poreux, il est très drainant (il laisse s'écouler l'eau). Il faut donc l'arroser régulièrement et surveiller l'humidité de la lasagne, particulièrement au début de la culture, le temps que les plants deviennent plus autonomes en eau.

Respecter les distances de plantation comme pour dans le jardin classique. Apporter un peu de compost à chaque plant.&#x20;



