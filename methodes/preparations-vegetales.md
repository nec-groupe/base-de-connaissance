---
description: Purins, décoctions, infusions, etc.
---

# Préparations végétales

Leur fonction est de renforcer la robustesse des plantes, alimenter leur croissance, et les renforcer contre les parasites et les pathogènes.

#### Purins d'orties, de consoude, de prêle :&#x20;

On peut faire un cocktail de purin avec ces trois plantes : l'ortie stimulera la croissance, la consoude luttera contre les champignons, la prêle renforcera les plantes et éloignera les insectes car elle est très riche en silice.

Dans un récipient en plastique (pas en métal), plonger 1kg de plantes fraîches dans 10L d'eau de pluie (ou du robinet si on la laisse reposer 24h pour éliminer le chlore) et laisser macérer pendant 3 semaines. Diluer 10 fois et utiliser en arrosage (en préventif et au moment de la plantation) ou en pulvérisation (lorsque les plants sont déjà installés).&#x20;
