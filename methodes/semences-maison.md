---
description: Comment produire ses propres semences ?
---

# Semences maison

Voir dans le guide MSV 2022, page 68

Sélection des graines les plus robustes aux évènements climatiques et météorologiques extrêmes : canicule, manque d'eau, pluies torrentielles, etc.&#x20;
