---
description: >-
  C'est l'art de se débarrasser des mauvaises herbes, aussi appellées
  adventices.
---

# Désherbage

### Différentes techniques :&#x20;

Bons outils, bonnes techniques, prévenir au lieu de subir : cultiver pour ne pas laisser la place aux mauvaises herbes (cultures et couvertures végétales, en plus du paillis). L'espacement serré des cultures, en plus de donner plus de récoltes, donne moins d'espace aux adventices. Compost sans mauvaise graine. La transplantation évite aussi que les adventices prospèrent. [Sarcler, biner,](binage-sarclage.md) ne jamais laisser monter en graines les mauvaises herbes.&#x20;

L'assiduité au désherbage est le seul moyen de diminuer la "banque de graines" présente dans le sol.&#x20;

### Technique du faux semis

Préparer la planche environ deux semaines avant de semer la culture, dans le but de laisser les mauvaises herbes qui viennent de la surface du sol (5cm) pousser. Juste avant de semer ou planter, il faut se débarrasser de ces mauvaises herbes avant leur floraison : en les fauchant, en les arranchant ou en les couchant au sol à l'aide de bois ou de pierres. Dans tous les cas, il est nécessaire de ne pas remuer le sol pour ne pas ramener en surface des graines qui repousseront. Apporter autant de soin au faux semis qu'à la culture : arrosage, éventuellement couverture flottante pour la chaleur, etc.&#x20;

### Bâche

Mettre une bâche sur une planche permet d'empêcher le soleil d'arriver aux adventices ou aux résidus de culture, ce qui les tue. Les mauvaises herbes germent rapidement sous la bâche, avec la chaleur et l'humidité, et sont ensuite détruites par l'absence de lumière. Pour tout ce qui se plante à plus de 50x50cm (pomme de terre, choux, tomates, poivrons, aubergines etc), on peut planter dans la bâche 1 mois après l'avoir posée. La bâche est un bon équilibre entre économie d'effort et rendement. Il est préférable d'avoir une bâche qui est traitée contre les rayons UV. &#x20;

