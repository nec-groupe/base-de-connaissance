---
description: .
---

# Paillage

Le paillage est aussi appellé paillis, mulch (paillis en anglais) ou mulching (paillage en anglais).&#x20;

On peut couvrir le sol avec de la matière minérale (pierre, cailloux, gravier, ardoise, etc, assez adapté aux plantes pérennes, arbustes, etc) ou organique. Avec une couverture organique vivante, le paillage est en réalité un [engrais vert](../engrais-verts/). On va donc décrire ici principalement les paillages "morts", en BRF (bois raméal fragmenté), paille, feuilles mortes, branches, etc, qu'on laisse sur le sol pour qu'ils se décomposent en 3 à 8 mois (en fonction de la nature du paillage et des conditions climatiques).&#x20;

Le paillage, comme les engrais verts, sert à couvrir le sol pour le protéger de l'érosion (dûe à la pluie, au soleil, au vent, etc,) à éviter que des mauvaises herbes poussent (bien que certaines poussent quand même), à nourrir la vie du sol (grâce à la matière organique, qui sert d'engrais et d'abri aux animaux), à conserver l'humidité (par stockage dans le paillage, mais aussi par condensation, et pour éviter l'évaporation à la surface du sol), à réguler la température du sol (il y a beaucoup moins de variations de température à la surface du sol (sous le paillage) s'il est paillé, car la paillage joue le rôle d'un isolant et protège le sol aussi bien du froid en hiver que de la chaleur en été), à éviter de tasser le sol. De plus, le paillis, comme l'incorporation de compost ou la présence d'humus dans le sol, permet de cimenter les particules pour les sols légers ([structure](../sol/structure.md) particulaire) et de diminuer l'adhésivité des sols lourds ([structure](../sol/structure.md) compacte) en les rendant plus friables et plus perméables à l'eau et à l'air.

Les quantités de paillages à appliquer dépendent du type de paillage et du type de cultures (20cm pour de la paille, 3-5cm pour un fumier pailleux). On peut laisser des grandes plantes (courge, tomate, etc) sous une grande hauteur de paillis, mais il faut éviter de faire la même chose avec des salades ou des radis, qui auront plus de mal à survivre dans ces conditions.&#x20;
