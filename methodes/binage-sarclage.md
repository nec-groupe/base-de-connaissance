---
description: .
---

# Binage/sarclage

### Binage

Le binage sert à entretenir les cultures et à leur rendre disponible l'eau. "Un binage vaut deux arrosages". Le binage a pour effet de casser la croute de battance du sol après une pluie (micro-vaisseaux capillaires qui se forment à la surface du sol, et par lesquels l'eau s'évapore), d'aérer la terre et de faciliter les échanges gazeux au niveau des racines. Il active une remontée de l'eau disponible aux racines superficielles. Attention cependant à bien équilibrer la profondeur du binage pour qu'il soit efficace, sans pour autant endommager les racines superficielles des cultures.&#x20;

### Sarclage

C'est la même chose que le binage, mais plus en surface : on le fait quand l'herbe est au stade de plantule. (C'est mieux de bier lorsque les herbes sont bien intallées, ou lorsque la terre est bien tassée par la pluie.) Le sarclage brise le "croutage" de surface et permet d'aérer le sol.&#x20;



Source principale : [https://www.youtube.com/watch?v=A33GsWtUbCs](https://www.youtube.com/watch?v=A33GsWtUbCs)
