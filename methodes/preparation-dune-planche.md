---
description: >-
  Une planche permanente est un planche qu'on garde indéfiniment. C'est ce qu'on
  fait à Mauves.
---

# Planche permanente

La planche permanente permet l'appronfondissement des systèmes racinaires, car la terre est ameublie, jamais tassée, et on re-cultive dessus année après année. C'est l'idéal pour bâtir et entretenir un sol. Les planches ne sont pas compactées, elles permettent une densification des cultures (voir [Culture bio-intensive](../techniques-de-cultures/culture-bio-intensive.md)) et la concentration des amendements sur les planches.&#x20;

Les planches butées (légèrement surélevées) ont un meilleur drainage (ce qui est particulièrement utile pour les climats nordiques comme au Québec) elles se réchauffent plus vite au printemps.



## Mise en culture : préparation

1. Passage de la [houe.md](../outillage/houe.md "mention") pour enlever la couche de terre et de végétaux de surface.
2. Démotter les herbes et les déposer hors des[jardin.md](../organisation-des-cultures/organisation-spatiale/jardin.md "mention")s&#x20;
3. Creuser sur **5 cm** les [passe-pied.md](../organisation-des-cultures/organisation-spatiale/passe-pied.md "mention")s à la [houe.md](../outillage/houe.md "mention")&#x20;
4. Passer la [grelinette.md](../outillage/grelinette.md "mention")
5. Briser les mottes avec une [griffe-piocheuse-3-dents.md](../outillage/griffe-piocheuse-3-dents.md "mention")
6. Égaliser la planche avec un [rateau.md](../outillage/rateau.md "mention")

## Remise en culture

Cela reste à découvrir.
