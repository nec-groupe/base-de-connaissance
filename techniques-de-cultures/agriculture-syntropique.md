---
description: .
---

# Agriculture syntropique

4 piliers de l'agriculture syntropique : planter plusieurs espèces au même endroit (polyculture, [associations de culture](../plantes/associations-de-culture.md)), stratification (besoin d'ombre ou de lumière pour les différentes plantes), succession dans le temps (ne jamais laisser d'endroit vide sans culture et anticiper), perturbation (couper régulièrement des branches, etc, pour forcer les plantes à pousser encore plus).
