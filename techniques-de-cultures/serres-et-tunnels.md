---
description: .
---

# Serres et tunnels

Une serre est chauffée, pas un tunnel.

Une serre peut servir de pépinière au printemps, tandis qu'un tunnel peut prolonger la saison en automne. Les deux servent à abriter et à favoriser des cultures de chaleur en été (poivrons, tomates, concombres, etc.). Il faut controller la ventilation plusieurs fois par jour au printemps et à l'automne. Ils doivent être proches des autres intallations fréquentées.&#x20;

Une orientation Nord/Sud va répartir la lumière pendant la saison de production, tandis qu'une orientation Est/Ouest va capter un maximum de lumière de septembre à mars, quand le soleil est bas, pour prolonger la saison.&#x20;

Pour éviter l'ombre d'une de ces structures sur une autre, il faut prévoir entre les deux un espacement égal à la largeur d'une de ces structures.&#x20;

