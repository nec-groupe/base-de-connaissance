---
description: >-
  Une forêt-jardin est un modèle d'agriculture basé sur le fonctionnement d'une
  forêt. Elle est dense, diversifiée, demande peu de travail une fois mise en
  place.
---

# Forêt-jardin

Une forêt-jardin est composée de plusieurs étages de production annuelle, et on va y produire divers produits selon les étages : des épices, des fruits, des champignons, des fibres, des matériaux de construction, etc. N'importe quelle plante a sa place dans une forêt-jardin, mais l'agencement est très important.&#x20;

On utilise donc la troisième dimension : la verticalité. Il y a 7 strates de végétation : arbres hauts, petits arbres, arbustes, plantes herbacées, couvre-sols, légumes-racines, champignons, plus la strate verticale (lianes).&#x20;

La forêt-jardin est adaptée aux terrains en pente grâce à sa non-mécanisation. Le rendement d'une forêt-jardin permet de nourrir environ 6 personnes par hectare.

#### Mise en place :&#x20;

Réaliser les aménagements et les terrassements (y compris une éventuelle [mare](../milieux/mare.md)) avant de planter, pour ne pas abimer les plantes. Ne pas tasser le sol. Faire les chemins en premier et les utiliser durant la création du jardin. Planter les grands arbres en premier, puis les arbustes, etc. Laisser des espaces libres pour créer des sous-bois, et diversifier au maximum les espèces de plantes (pour augmenter sa résilience et avoir des récoltes tout au long de l'année). Semer les prairies et les céréales après les plants d'arbres pour éviter de marcher dessus lors des plantations.

Faux-semis, ratissage, [grelinette](../outillage/grelinette.md), puis semis avec, en plus des graines, du sable non-calcaire et du compost tamisé à parts égales (un demi-litre par m² avec les graines (en calculant le dosage par m²), puis recouvrir de sable et de compost à raison de 3L par m²).

Pailler les jeunes plants avec des branches ou des feuilles pour créer une litière, rester attentif aux sécheresses





C'est un modèle de culture qui se rapproche de l'[agriculture syntropique](agriculture-syntropique.md) par certains aspects. La forêt-jardin se différencie de l'[agroforesterie](agroforesterie.md), car ce dernier comporte seulement 2 ou 3 strates. &#x20;

