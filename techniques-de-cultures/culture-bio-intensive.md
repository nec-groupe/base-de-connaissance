---
description: .
---

# Bio-intensif

Référence principale : Jean-Martin Fortier, le [jardinier-maraîcher](../bibliotheque/le-jardinier-maraicher.md).

Référence d'expérience et d'innovation en maraîchage diversifié sur petite surface (selon J.-M. Fortier) : Eliot Coleman

"Savoir obtenir de la terre le meilleur rendement sans dépense excessive, par le choix judicieux des cultures et à l'aide de travaux appropriés, tel est le but du jardinier-maraîcher."

Une des bases du bio-intensif est l'espacement serré des cultures, qui joue le rôle de paillis vivant : cela crée un microclimat entre les plantes, leur permet de mieux résister au vent et ainsi à l'évapotranspiration, cela diminue l'évaporation de l'eau du sol, et cela empêche les mauvaises herbes de pousser entre les cultures à cause de leur ombre. L'extrêmité des feuilles des cultures doivent se toucher lorsque la plante est au trois quarts de sa croissance.

L'espacement est aussi serré dans le temps, les cultures (ou en tous cas la couverture vivante du sol par des plantes) doivent se succéder au maximum.&#x20;

Les [planches permanentes](../methodes/preparation-dune-planche.md) sont aussi une base de la culture en bio-intensif. Elles sont enrichies en matière organique, ameublies à la grelinette. Les systèmes racinaires des cultures ne se gênent pas, même si les plants sont serrés.

Préparation d'une planche au milieu de l'été : engrais verts et résidus de culture sous une bâche noire pendant 2-3 semaines, puis grelinette pour aérer le sol, puis mettre les amendements et un coup de rateau, puis semis.&#x20;
