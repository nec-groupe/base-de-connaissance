---
description: Liste de vidéos intéressantes à voir.
---

# 🎞 Vidéographie, sites

Interview de Lydia et Claude Bourguignon, ingénieurs de terrain spécialisés dans l'étude du sol, par Thinkerview, 2h25, 2023 [https://www.youtube.com/watch?v=OWnT1bkHQto](https://www.youtube.com/watch?v=OWnT1bkHQto)

Ver de Terre Production, une chaine youtube très active et apparemment très complète sur l'agriculture [https://www.youtube.com/@VerdeTerreProduction/videos](https://www.youtube.com/@VerdeTerreProduction/videos)

Projet d'autosuffisance alimentaire et énergétique sur un hectare dans le Périgord : [https://pierre1911.blogspot.com/](https://pierre1911.blogspot.com/)\
