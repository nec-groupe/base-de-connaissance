---
description: .
---

# Mélanges améliorants

## Mellifère et améliorant&#x20;

**20 g** pour **10 m²**, ce qui donne **un sachet** par [planche.md](../organisation-des-cultures/organisation-spatiale/planche.md "mention").

{% embed url="https://www.fermedesaintemarthe.com/melange-fleuri-mellifere-et-ameliorant-ab-20-g-p-20843" %}
Attirer la biodiversité
{% endembed %}

{% embed url="https://www.fermedesaintemarthe.com/melange-hiver-ameliorant-du-sol-ab-150-g-p-21013" %}
Nourrir le sol pendant l'hiver
{% endembed %}

{% embed url="https://www.fermedesaintemarthe.com/melange-maraicher-ameliorant-du-sol-ab-150-g-p-20876" %}
Nourrir le sol pendant l'été
{% endembed %}

## LMS fin de saison

Densité de semis : **15 à 20 g/m²**, sur [kokopelli.md](../semenciers/kokopelli.md "mention"), il est vendu par **sachet de 120 g.** Si l'on plante à densité max, ça fait **2 sachets par planche.**

{% embed url="https://kokopelli-semences.fr/fr/p/M0059-Legumineuse-Meteil-Seigle" %}
