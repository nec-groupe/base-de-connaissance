---
description: .
---

# Lin

Le lin bleu cultivé, _Linum usitatissimum_, est une très ancienne espèce annuelle. Depuis des milliers d’années, les peuples d’Asie Centrale, les Égyptiens, les Grecs et les Gaulois favorisent le développement d’une espèce de lin nommée _usitatissimum_. La première fibre, âgée de 36 000 ans, fut trouvée dans la grotte de Dzudzuana en Géorgie.

Cette plante aux tiges fines mesure de 80 à 120 cm de hauteur. Elle offre d’élégantes petites fleurs comestibles de couleur bleu clair, de 2 à 3 cm de diamètre, formées de 5 pétales en entonnoir. Après fécondation, chacune de ces fleurs délicates, à la durée de vie très courte, laisse place à une capsule contenant 2 graines.

Peu exigeant et de croissance rapide, le lin se cultive souvent comme engrais vert de printemps. Il améliore la structure du sol et décompacte les terres lourdes grâce à la densité importante de son système racinaire. Ses tiges, très riches en carbone, constituent un excellent paillage.

Associé à des fleurs de [cosmos](https://kokopelli-semences.fr/fr/c/semences/plantes-diverses/cosmos) et des [nigelles](https://kokopelli-semences.fr/fr/c/semences/plantes-diverses/nigelle), ce lin bleu habille joliment le jardin potager et floral. Il se plaî​t également en pot et en jardinières, sur les balcons et terrasses.

{% embed url="https://kokopelli-semences.fr/fr/p/F0349-Cultive" %}

Densité de semis 5 à 10 g/m². Vendu par sachet de 70 g, **un sachet** par [planche.md](../organisation-des-cultures/organisation-spatiale/planche.md "mention").

## Sources

{% embed url="https://kokopelli-semences.fr/fr/p/F0349-Cultive" %}
