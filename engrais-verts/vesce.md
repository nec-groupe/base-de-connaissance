---
description: .
---

# Vesce



<figure><img src="../.gitbook/assets/vesce3.jpg" alt=""><figcaption></figcaption></figure>

## Comment semer la vesce ?

**La vesce se sème à la volée** puis on recouvre les graines d’environ 1 cm de terre en nivelant au râteau. Tassez légèrement et arrosez.

> La vesce étant une plante grimpante donc versante, il est conseillé de la semer avec du seigle ou de l’avoine.

Ainsi, les tiges de ces céréales seront utilisées comme tuteurs par la vesce. Elle germe **au bout de 10 à 20 jours**.\
\
Densité de semis de **5 g** par **m².**&#x20;

## Variétés

## Vesce d'été

Quand faucher la vesce ? Le meilleur moment pour faucher est **juste avant la floraison** lorsque les **fleurs sont en bouton**. C'est à ce stade que la plante est la plus concentrée en éléments nutritifs et surtout en matières azotées, et sa décomposition dans le sol sera plus rapide que si vous fauchiez plus tard.

<table><thead><tr><th width="59.333333333333314">J</th><th width="44">F</th><th width="47">M</th><th width="45">A</th><th width="45">M</th><th width="40">J</th><th width="42">J</th><th width="45">A</th><th width="43">S</th><th width="46">O</th><th width="47">N</th><th width="47">D</th></tr></thead><tbody><tr><td></td><td></td><td>S</td><td>-</td><td>R</td><td>R</td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td>S</td><td>-</td><td>R</td><td>R</td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td>S</td><td>-</td><td>R</td><td>R</td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td>S</td><td>-</td><td>R</td><td>R</td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td>S</td><td>-</td><td>R</td><td>R</td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>s</td><td>-</td><td>R</td><td>R</td><td></td></tr></tbody></table>

{% embed url="https://kokopelli-semences.fr/fr/p/V0007-Vesce-d-Ete" %}

Deux sachets de 80g pour 3 [planche.md](../organisation-des-cultures/organisation-spatiale/planche.md "mention")s.



## Vesce d'hiver

La vesce d'hiver sera éliminée par le gel et directement incorporée au sol, pas besoin de la faucher.

<table><thead><tr><th width="59.333333333333314">J</th><th width="44">F</th><th width="47">M</th><th width="45">A</th><th width="45">M</th><th width="40">J</th><th width="42">J</th><th width="45">A</th><th width="43">S</th><th width="46">O</th><th width="47">N</th><th width="47">D</th></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>S</td><td>S</td><td>S</td><td>-</td><td>-</td></tr></tbody></table>

{% embed url="https://kokopelli-semences.fr/fr/p/V0008-Vesce-d-Hiver" %}
