---
description: .
---

# Phacélie

La phacélie (_Phacelia tanacetifolia_) fait partie de la famille des Hydrophyllacées.

Densité de semis : 1 g par m²&#x20;

| Avantages                                           | Inconvénients                                   |
| --------------------------------------------------- | ----------------------------------------------- |
| Fleurs très mellifères                              |  Peut être hôte du virus Y de la pomme de terre |
| Croissance rapide                                   |  Peut abriter des ravageurs (pucerons, thrips)  |
| Rotation idéale (pas de légumes de la même famille) |  Supporte assez mal la chaleur                  |

{% embed url="https://kokopelli-semences.fr/fr/p/F0257-A-Feuilles-de-Tanaisie" %}

Sachet de 60 g pour 1 m². Un sachet = 6 planches

