---
description: >-
  Un engrais vert est une plante semée sur une parcelle nue, entre deux
  cultures, destinée à être fauchée pour nourrir le sol.
---

# 🪴 Engrais verts

### À quoi sert un engrais vert ?

Un engrais vert sert à couvrir le sol (pour le protéger du froid, de la pluie, du soleil, de l'érosion, etc), à éviter que des mauvaises herbes poussent (l'engrais vert doit alors pousser plus vite que les mauvaises herbes), à nourrir le sol, le décompacter à l'aide de leurs racines à différentes profondeurs, entretenir la vie du sol, à conserver l'humidité, reconstituer les réserves nutritives du sol, à attirer les insectes mellifères, etc. Ses fonctions dépendent en partie du type d'engrais vert choisi. Les engrais verts peuvent être en même temps des [plantes compagnons](../plantes-compagnon/).

Il faut semer les cultures de couverture à une densité très élevée (5 à 10 fois le taux recommandé), pour protéger bien et rapidement le sol et gagner du temps en désherbage.&#x20;

### **Quel engrais vert choisir ?**

Le choix de l’engrais vert se fait en fonction de plusieurs critères :

* la période de semis,
* **l’effet recherché (apport d’azote, décompactage…),**
* la **culture envisagée à la suite** **de l’engrais vert**, car celui-ci entre en compte dans le système de rotation des cultures. Ainsi, avant l’installation de choux, on évitera d’utiliser la Moutarde comme toutes deux sont des Brassicacées. La Phacélie est très souple d’utilisation, parce qu'elle appartient à la famille des Hydrophyllacées, dont aucun légume ne fait partie.

<table data-header-hidden><thead><tr><th width="120">Plante</th><th width="129">Famille botanique</th><th width="96">Cycle</th><th width="119">Semis</th><th>Intérêt</th></tr></thead><tbody><tr><td>Cameline</td><td>Brassicacée</td><td>annuelle</td><td>Avril à juin</td><td>Décompacte, aère les sols, fait barrage aux adventices - rustique, adapté aux sols pauvres</td></tr><tr><td>Fenugrec</td><td>Fabacée (légumineuse)</td><td>annuelle</td><td>fin mars à fin juillet</td><td>Il enrichit le sol en azote - adapté aux sols calcaires et aux climats secs</td></tr><tr><td>Luzerne</td><td>Fabacée (légumineuse)</td><td>vivace</td><td>Mars à septembre</td><td>Pour implantation de longue durée, décompactant</td></tr><tr><td>Moutarde blanche</td><td>Brassicacée</td><td>annuelle</td><td>mars à septembre</td><td>Piège les nitrates, croissance rapide, étouffe les adventices</td></tr><tr><td>Phacélie</td><td>Hydrophyllacées</td><td>annuel</td><td>Avril à août</td><td>structure le sol - mellifère</td></tr><tr><td>Sainfoin</td><td>Fabacée (légumineuse)</td><td>vivace</td><td>Mars à juin</td><td>très rustique (- 10 °) - fixe l’azote atmosphérique - idéal en sols superficiels, calcaires et secs</td></tr><tr><td>Sarrasin ou Blé noir</td><td>Polygonacée</td><td>annuelle</td><td>Mai à juin</td><td>nettoyant, décompactant, parfait en sol lourd, acide et en climat humide</td></tr><tr><td>Trèfle blanc, Trèfle violet</td><td>Fabacée (légumineuse)</td><td>vivace</td><td>Mars à mai</td><td>couvert longue durée, fixe l’azote dans le sol, mellifère</td></tr><tr><td>Trèfle incarnat</td><td>Fabacée (légumineuse)</td><td>annuelle</td><td>Août à septembre</td><td>fixe l’azote, mellifère, jolie floraison rouge</td></tr><tr><td>Seigle</td><td>Poacée (Graminée)</td><td>bisannuelle</td><td>Septembre à novembre</td><td>nettoyant, bon couvert végétal pour l’hiver</td></tr><tr><td>Vesce de Printemps</td><td>Fabacée (légumineuse)</td><td>annuelle</td><td>Mars à avril</td><td>végétation abondante - fixe l’azote atmosphérique, jolie floraison bleue</td></tr><tr><td>Vesce de Cerdagne</td><td>Fabacée (légumineuse)</td><td>annuelle</td><td>Août à octobre</td><td>Il convient aux sols lourds et pauvres</td></tr></tbody></table>

#### Engrais verts intercalaires :&#x20;

Avant ou après une culture principale, de sorte que les 2 cultures cohabitent. On peut semer du trèfle avant de récolter les carottes, par exemple, et le trèfle protègera la culture suivante. C'est une technique difficile à mettre en place, mais prometteuse.&#x20;

## **En pratique : le semis et la destruction de l’engrais vert**

Le semis des engrais verts est **simple et rapide**. Peu exigeants,  ils se contentent d’un sol rapidement décompacté, grossièrement ratissé. Le semis s’effectue **à la volée**, en respectant la quantité de graine par m² préconisée. La culture ne nécessite généralement pas d’arrosage.

La destruction de l’engrais vert se fait **naturellement par le gel ou par fauchage** plus ou moins rapide (dans tous les cas, avant la formation des graines… pour vous éviter les semis spontanés).

Une fois détruit, l’engrais vert pourra, au choix, être :

* **broyé puis incorporé aux couches superficielles du sol** (on n’enterre pas la matière organique, au risque de voir apparaître les taupins), la culture suivante n’est alors possible que trois semaines plus tard. Il est préférable de le recouvrir avec la terre des allées pour ne pas perturber la structure du sol.&#x20;
* **laissé sur place** en tant que paillage, il se décomposera sur place en quelques semaines,
* **ramassé et apporté au compost**, pour une mise en culture rapide de la parcelle.

Sources : \
[https://www.promessedefleurs.com/conseil-plantes-jardin/blog/conseils/engrais-verts-tout-ce-que-vous-avez-toujours-voulu-savoir](https://www.promessedefleurs.com/conseil-plantes-jardin/blog/conseils/engrais-verts-tout-ce-que-vous-avez-toujours-voulu-savoir)

#### Couverts permanents, couverts spontanés : à creuser
