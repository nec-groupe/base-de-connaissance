---
description: .
---

# 🏈 Fève

## Variétés&#x20;

### Kokopelli

Chez [kokopelli.md](../semenciers/kokopelli.md "mention"), on trouve six variétés de fèves. Les graines sont vendues par **sachets de 100 g**.&#x20;

Dans notre configuration, il nous faut **\~300 g** par [planche.md](../organisation-des-cultures/organisation-spatiale/planche.md "mention").

{% embed url="https://kokopelli-semences.fr/fr/c/semences/potageres/legumes-fruits/feves" %}

### Fève Wiktem

Cette variété, à port dressé, produit des gousses pouvant atteindre 20 cm de long, renfermant des grains, très appréciés crus, à demi-maturité, de 2,5 cm, de couleur blanche devenant brune à la cuisson.

{% embed url="https://kokopelli-semences.fr/fr/p/P8806-Witkiem" %}

## Plante compagnon

[aneth.md](../plantes-compagnon/annuelles/aneth.md "mention")\
