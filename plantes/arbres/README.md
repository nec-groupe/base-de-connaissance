---
description: .
---

# 🌲 Arbres

### Baissière

Une baissière est une petite piscine autour d'un arbre, de quelques cm de profondeur, qui permet à l'eau de pluie de s'accumuler et de s'infiltrer en profondeur dans le sol sous l'arbre. Ça peut être utile pour des fruitiers qui ont des racines profondes, par exemple. La baissière est adaptée pour les terrains en pente où l'eau ruisselle à chaque pluie torrentielle, ou pour les climats secs.&#x20;
