---
description: 'Source principale : https://www.youtube.com/watch?v=pfUK_ADTvgY'
---

# Greffes

Une greffe consiste à assembler une branche d'un arbre (qu'on appelle le greffon) sur une base d'un autre arbre (qu'on appelle le porte-greffe), dans le but de profiter des avantages du système racinaire du porte-greffe et de la production du donneur de greffon.&#x20;

L'avantage de la greffe est ainsi de cultiver des arbres fruitiers sauvages résistants aux maladies et à la sécheresse, n'ayant pas besoin d'entretien. Les fruits viennent plus rapidement : 2 à 5 ans pour un poirier au lieu de 5 à 8 ans, par exemple.&#x20;

La greffe a un intérêt non-négligeable pour éviter les désavantages de certains types de sol par rapport à la plante : par exemple, un amandier ne poussera pas sur un sol gorgé d'eau, mais un amandier greffé sur un prunier, qui aime bien ces sols, poussera bien et donnera des amandes. Pareil pour faire pousser des abricots sur un sol acide sur un porte-greffe de prunier ou de pêcher.&#x20;

On ne peut pas greffer n'importe quoi sur n'importe quoi, il existe des tableaux de compatibilité. On commence généralement en mars par les arbres à noyaux, puis ensuite avec les arbres à pépins, qui démarrent plus tard.

### Décembre, janvier, février :&#x20;

On peut en récupérer certains à floraisons précoces ou tardives pour varier les récoltes, et en tous les cas, sur des arbres qui donnent bien pour maximiser les récoltes. Récupérer des greffons, c'est-à-dire des jeunes pousses, des jeunes branches de l'année. Les greffons doivent être des bouts de branches qui produisent les fruits, la partie aérienne, penchées à 45°. Les branches qui regardent vers le bas sur l'arbre auront plus de mal à démarrer, mais marcheront quand même. On peut conserver ces greffons dans du papier humide dans un sachet plastique au réfrigérateur, ou dans une poubelle de sable à l'ombre tout le temps, jusqu'en mars-avril.

### Mars, avril :&#x20;

On greffe. Matériel : bon sécateur, élastiques à greffer, bon couteau, mastic à greffer, pansements pour les coupures. Ne pas greffer quand il gèle ou quand il pleut, la greffe s'abimerait.&#x20;

Le porte-greffe fait des racines et permettra à l'arbre fruitier de pousser. Il faut couper les branches du porte-greffe qui gênent. Puis couper le tronc, pas trop bas pour pouvoir recommencer en cas d'échec et pour éviter les attques de lapins et de limaces (<20cm). Puis couper le greffon en biseau, et faire en sorte que le cambium (partie verte sous l'écorce) du greffon touche le cambium du porte-greffe. Fendre le tronc du porte-greffe avec un couteau, puis insérer le greffon deddans, sur un côté pour que les cambiums se touchent. Puis intaller l'élastique à greffer sur la greffe, en commençant par le bas pour éviter que l'eau rentre à l'intérieur. Puis mettre du mastic à greffer en haut du greffon et autour de la greffe, là où le bois est ouvert, jusqu'en bas de la fissure.&#x20;

On peut laisser entre 1 et 3 yeux sur le greffon, en fonction de la taille du porte-greffe. Planter une des branches coupées à côté du porte-greffe pour accueillir les oiseaux qui risquent d'abimer la greffe.

### Mai, juin :&#x20;

On nettoie les porte-greffe. Les greffes ayant réussi démarrent, les bourgeons s'ouvrent et des feuilles aparaissent. Il faut enlever tout ce qui pousse en-dessous du point de greffe, pour donner un maximum d'énergie à la greffe. S'adapter aux attaques des animaux sauvages, en faisant des greffes en hauteur pour éviter les chevreuils par exemple.&#x20;

### Juin, juillet :&#x20;

On enlève les élastiques.

### Août :&#x20;

On élimine les rejets sous le point de greffe, pour les mêmes raisons qu'en mai-juin, surtout pour les jeunes arbres, qui font beaucoup de rejets. De même pour les rejets racinaires, qui aparaissent à partir des racines du porte-greffe à quelques mètres de l'arbre.
