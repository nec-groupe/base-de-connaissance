---
description: >-
  Une trogne est un arbre auquel on coupe les branches à ras régulièrement. Cela
  a plusieurs intérêts.
---

# Trognes

Les trognes servent à produire de la matière organique, car on coupe les branches régulièrement, ce qui peut servir à alimenter les cultures ou le compost, par exemple. Comme le réseau racinaire des trognes est très développé (contrairement aux branches, elles ne sont pas limitées dans leur croissance au fil des années), ces branches poussent très vigoureusement. Et d'autant plus qu'on laisse la "boule" intacte en haut de l'arbre, qui permet de produire un maximum de biomasse.&#x20;

Par ailleurs, le réseau racinaire des trognes est très utile aux cultures voisines, car elles apportent de la matière organique directement dans le sol (via des champignons qui décomposent les racines inutiles et mortes, ainsi que les matières organiques en général), ainsi que de l'eau (via ces mêmes champignons qui transportent l'eau dans le sol. Voir [champignons mycorhiziens](../../sol/champignons-mycorhiziens.md)). Il est préférable que la trogne ait un réseau racinaire relativement superficiel dans le but de redistribuer l'eau et la matière organique aux cultures à côté.&#x20;

Les trognes permettent également de décompacter le sol, et de nourrir la vie du sol.&#x20;

Également, c'est un lieu de vie et d'abri utile aux oiseaux, insectes et divers animaux qui sont autour : les trognes, comme les autres arbres, sont bénéfiques pour la biodiversité, donc pour les cultures.&#x20;
