---
description: Ce sont des arbres dont on peut récupérer les fruits pour les consommer.
---

# Arbres fruitiers

### Fruitiers de plein vent

Grands pommiers, poiriers et cerisiers de l'espace champêtre, des grandes haies bocagères , jamais taillés ou très peu.

### Fruitiers productifs

Plus petits que les fruitiers de plein vent, souvent greffés sur des porte-greffes limitant leur croissance pour faciliter la récolte, et taillés tous les deux ans. Cerisiers, poiriers, pommiers, abricotiers, pruniers, figuier, etc.&#x20;
