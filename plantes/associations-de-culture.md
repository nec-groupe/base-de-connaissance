---
description: .
---

# Associations de culture

### Milpa (ou "trois soeurs d'Amérique")

Association composée de maïs, de haricots et de courges, utilisée depuis longtemps par les habitants d'Amérique centrale et du sud.&#x20;

Le maïs sert de tuteur pour le haricot, et la courge amène de l’ombre aux pieds et couvre le sol. Le haricot apporte de l’azote au maïs. Maïs-haricot-maïs-courge, répéter.
