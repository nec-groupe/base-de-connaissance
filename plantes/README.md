---
description: .
---

# 🥦 Cultures

On peut diversifier les espèces et les variétés d'une même culture (précoces, tardives, etc) pour étaler les rendements dans le temps. Pour ça, on peut aussi tailler intelligemment certaines cultures comme les melons : tailler la moitié des rameaux du melon permet d'avoir des fruits plus tôt sur les rameaux taillés, et plus tard sur les rameaux non-taillés.

Dans certains cas, on peut laisser monter en graine la culture pour qu'elle se re-sème spontanément d'une année sur l'autre. Dans ce cas, il est préférable de favoriser les plantes les plus fortes et les mieux adaptées pour laisser la séléction opérer. Les légumes-feuilles, les apiacées (persil, aneth, coriandre, fenouil, etc), les aromatiques herbacées sont enclines au semis spontané, mais pas les [cucurbitacées](cultures/cucurbitacees/) ni les [brassicacées](cultures/brassicacees.md), à cause des risques d'hybridation.&#x20;

Les sous-catégories de cet article sont classées par familles de cultures (brassicacées, légumineuses, etc.) Il est aussi possible de parler des cultures par type : feuille, bulbe, racine, fruit, graine ou fleur. Ces deux façons de classer les cultures se recoupent, on va donc dire ici tout ce qu'il est pertinent de dire en rapport avec les types de cultures, mais pas forcément en rapport avec telle ou telle famille.

### Feuille

Les légumes-feuilles demandent beaucoup d'arrosage, car ils ont une grande surface foliaire et sont donc plus sujets à l'évapotranspiration (évaporation d'eau à la surface des feuilles), et ils ont un système racinaire superficiel qui ne permet pas d'aller chercher des réserves d'eau profondes.

Ils ont besoin de beaucoup d'azote.

### Bulbe

Ils peuvent être rapprochés des légumes-racines.&#x20;

### Racine

Les légumes racine demandent peu d'arrosage : la partie essentielle de la plante se trouve sous terre, à l'abri des grosses déperditions d'eau, et la plante pousse principalement en-dehors de l'été et lorsque les réserves d'eau du sol sont suffisantes. Elles sont donc plus sensibles aux pourritures et maladies lorsqu'il y a trop d'eau.&#x20;

### Fruit

### Graine

Souvent (toujours ?) des [légumineuses](cultures/legumineuses.md).

### Fleur
