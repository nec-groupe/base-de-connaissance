---
description: >-
  Une haie est un ensemble de plantes ayant une certaine hauteur (~1-4m). Elle
  est permanente et a beaucoup d'utilités.
---

# 🎍 Haies

Une haie peut servir à empêcher le bétail de s'échapper, mais elle peut servir également à "casser" le vent et protéger les cultures d'un vent trop fort et trop permanent qui augmenterait beaucoup le phénomène d'évapotranspiration des feuilles (l'eau qui s'évapore des feuilles au contact de l'air), et donc la quantité d'eau dans le sol. Un mur bâti, quant à lui, qui ne fait que dévier les flux d'air mais ne les ralentit pas. Le vent réduit aussi la température et la production des plantes. Elle permet aussi d'intaller des microclimats dans le jardin, de délimiter des terrains, etc. Elle permet également d'éviter l'érosion des sols, en retenant la terre lors des pluies torrentielles, et en cassant le vent.&#x20;

Une haie permet également, et c'est sans doute là son plus grand avantage pour les cultures, d'accueillir de la biodiversité : petits animaux, oiseaux, insectes, abeilles, etc., en leur apportant de la nourriture, de quoi butiner, un abri, ce dont ils ont beaucoup besoin surtout en hiver.&#x20;

On peut également s'en servir comme production de bois, pour un paillage ou un amendement des cultures par exemple, ou pour produire des fruits (arbres fruitiers) et des champignons.&#x20;

Une haie efficace (en permaculture) est diversifiée au maximum, composée de plusieurs essences et familles différentes, au moins une dizaine, qui auront chacune des fonctions différentes : des essences locales, des essences sauvages, des arbres fruitiers, des grands arbustes (qui produisent des fruits immangeables en l'état, mais qu'on peut transformer pour les consommer), des petits arbustes (mûriers ronces, framboisiers, cassissiers, groseilliers, etc), des arbres ayant des feuilles persistantes, des arbutes grimpants (qui colonisent l'espace en hauteur et apportent des petits fruits aux oiseaux), des fixateurs d'azote, des essences fonctionnelles (mellifères, fruitières, etc.), des arbustes épineux (pour les nids des oiseaux), etc. Elle doit avoir plusieurs étages (petits arbres, grands arbres, arbustes, buissons, etc.) et des arbres plantés en quinconce, tous les 50cm environ.&#x20;

Certains arbres sont plus riches en nectar, d'autres en pollen, et ils apportent des choses différentes.&#x20;

Pour préparer une haie à partir de rien, l'idéal est de planter/semer de l'avoine, du sorgho, du tournesol, par exemple, qui sont des plantes qui poussent vite et haut, et qui produisent de la biomasse. En tous les cas, une haie efficace doit comporter des variétés complémentaires, être bien pensée et bien conçue. Toutes les plantes d'une haie, et surtout les fruitières, doivent avoir la place de s'épanouir, et recevoir suffisamment de soleil, et aussi avoir assez d'aération. Il faut choisir les essences et le plan en fonction du climat et du sol sur lequel la haie s'installe. En général, on plante un arbre pour 4 à 5 arbustes, mais ça peut varier.

#### Brise-vents artificiels

Une palissade ou un filet synthétique permettent de faire office de haie artificielle, avec l'avantage qu'ils peuvent être mis en place rapidement. Cependant, ils sont plus chers, moins hauts, moins durables, et n'offrent pas tous les avantages d'une haie végétale, qui reste l'idéal à long terme.&#x20;
