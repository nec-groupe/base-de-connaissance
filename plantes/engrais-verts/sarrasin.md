---
description: .
---

# Sarrasin

Lae sarrasin étouffe les mauvaises herbes en moins d'un mois. En plus de ça, c'est une très bonne nourriture pour le sol et cela donne des fleurs pour les abeilles. On peut l'utiliser en bouche-trou au milieu d'une saison.&#x20;
