---
description: .
---

# Pois

{% embed url="https://kokopelli-semences.fr/fr/c/semences/potageres/legumes-fruits/pois" %}

## Variétés&#x20;

Cette variété naine offre une abondance de gousses vert foncé renfermant des grains ridés, utilisés frais ou séchés.

Cette variété s’adapte à de nombreuses conditions de culture.

Sachet de **80 g, 2 sachets par planche**



{% embed url="https://kokopelli-semences.fr/fr/p/P9901-Merveille-de-Kelvedon" %}
