---
description: .
---

# 🍈 Melon



## Variétés&#x20;

### Kokopelli

Chez [kokopelli.md](../semenciers/kokopelli.md "mention"), on trouve 66 variétés de melons. Les graines sont vendues par **sachets de 25 graines**. Dans notre configuration, il nous faut **\~75 graines** par [planche.md](../organisation-des-cultures/organisation-spatiale/planche.md "mention").\


{% embed url="https://kokopelli-semences.fr/fr/c/semences/potageres/legumes-fruits/melons" %}

#### Tiger

Cette variété produit une abondance de fruits de 500 g à l’épiderme jaune éclatant rayé de rouge feu. Certains fruits restent uniquement jaunes. Leur chair blanche offre une bonne saveur sucrée et parfumée.\
\
Je me suis dit qu'il pourrait être intéressant d'avoir plus de fruits de plus petite taille pour partager facilement.

{% embed url="https://kokopelli-semences.fr/fr/p/P4356-Tiger" %}

#### Cantaloup Charentais

Cette variété ancienne produit des fruits ronds, lisses et de couleur vert pâle. Leur chair orange est juteuse et offre une saveur sucrée et très appréciée.

{% embed url="https://kokopelli-semences.fr/fr/p/P4328-Cantaloup-Charentais" %}

#### Mélange Creusois

Ce mélange, sélectionné à partir de variétés rustiques adaptées aux climats frais, a été nommé ainsi car les deux producteurs, Karine et Yoann, qui le multiplient sont basés en Creuse. Il ne s’agit pas de variétés originaires de la Creuse.

Ce mélange évolutif, à l'incroyable diversité, est idéal pour les passionnés de la création variétale libre, les curieux et les jardins les plus frais !

{% embed url="https://kokopelli-semences.fr/fr/p/L0394-Melange-Creusois" %}

## Engrais verts associés

[vesce.md](../engrais-verts/vesce.md "mention")[#vesce-dhiver](../engrais-verts/vesce.md#vesce-dhiver "mention")



## Plantes compagnons

Certaines variétés de[soucis.md](../plantes-compagnon/annuelles/soucis.md "mention") ou calendula repoussent les nématodes qui squattent les racines du melon.





Semis : en pleine terre, en godet

Conseil de semis :&#x20;

Semer en godets, à une température comprise entre 20 et 25 °C, 6 semaines avant la mise en place. Repiquer avec la motte, après les dernières gelées, dans des trous de plantation enrichis de compost, espacés de 1 m en tous sens.Un semis en pleine terre dans les mêmes conditions est possible lorsque le sol est bien réchauffé et que les températures extérieures ne descendent plus en dessous de 15 °C.

Conseil de culture :&#x20;

Le melon a besoin de beaucoup de lumière et de chaleur pour arriver à maturité avant les gelées.

Période de semis (sous abri) : Mars, Avril, Mai

Période de semis (pleine terre) : Avril, Mai

Période de récolte : Juillet, Août, Septembre, Octobre

Culture : en pleine terre, en serre

Exposition :&#x20;

ensoleillée

Besoin en eau :&#x20;

fort

Nature du sol :&#x20;

calcaire, humifère

Qualité du sol :&#x20;

réchauffé, riche, drainé, léger
