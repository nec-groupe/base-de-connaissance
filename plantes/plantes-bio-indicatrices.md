---
description: >-
  Une plante bio-indicatrice nous renseigne sur le type de sol, ses
  caractéristiques.
---

# Plantes bio-indicatrices

Par exemple, certaines plantes poussent mieux sur des sols acides ou sableux. Si ces plantes-là poussent sur un certain sol, alors ce sol est acide, ou sableux, etc.
