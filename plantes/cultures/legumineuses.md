---
description: Pois, lentilles, haricots, fèves, etc
---

# Légumineuses

Les légumineuses (aussi appellées fabacées) fixent l'azote de l'air pour le rendre disponible aux autres plantes à côté. Cela se fait par l'intermédiaire de bactéries (les rhiboziums) situées dans des petites boursouflures des racines. Certains sols qui n'ont pas vu de légumineuse depuis longtemps n'ont pas ces bactéries, on peut alors en inoculer dans le sol.&#x20;

Les légumineuses en engrais vert doivent être enfouis juste avant leur floraison. C'est à ce moment qu'ils ont emmagasiné le plus d'azote et qu'ils le rétribuent le plus vite.&#x20;
