---
description: C'est une culture qu'on ne fait pas (encore) à Mauves.
---

# Gingembre

En tunnel, pour une température haute mais pas trop : le gingembre aime l'humidité et la chaleur, y compris la nuit. Désherber régulièrement, pas d'engrais à cause de la salinité. Le gingembre pousse de mai à octobre, il est récolté en octobre et il est enfoui sous la terre (assez profond et avec les feuilles encore dessus) jusqu'en décembre pour conserver sa saveur et résister au gel.&#x20;

Source principale : Des épices saines, Arte, youtube, 2022
