---
description: Choux, navets, radis, etc
---

# Brassicacées

Les brassicacées extraient le phosphore (P) et et le potassium (K) du sol pour les rendre disponibles aux autres cultures.
