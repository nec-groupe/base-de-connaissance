---
description: .
---

# Laitues

## Variétés

Sur Kokopelli environ 100 variétés de graines différentes. Plusieurs varitétés ont été sélectionnées, à la fois des pommées et des laitues à couper (qui montent moins).&#x20;

### **Laitues pour le printemps :**&#x20;

Laitue à couper Redder Ruffled Oak : [https://kokopelli-semences.fr/fr/p/P3504-Redder-Ruffled-Oak](https://kokopelli-semences.fr/fr/p/P3504-Redder-Ruffled-Oak) \
Laitue à couper Redina : [https://kokopelli-semences.fr/fr/p/P4146-Redina](https://kokopelli-semences.fr/fr/p/P4146-Redina) \
Laitue pommée Reine de Mai : [https://kokopelli-semences.fr/fr/p/P3704-Reine-de-Mai](https://kokopelli-semences.fr/fr/p/P3704-Reine-de-Mai) \
Laitue pommée Gloire de Nantes : [https://kokopelli-semences.fr/fr/p/P3734-Gloire-de-Nantes](https://kokopelli-semences.fr/fr/p/P3734-Gloire-de-Nantes)&#x20;

### **Laitues pour l'été (pas dans la rotation mais pourquoi pas) :**&#x20;

Laitue pommée Reine de Juillet : [https://kokopelli-semences.fr/fr/p/P3822-Reine-de-Juillet](https://kokopelli-semences.fr/fr/p/P3822-Reine-de-Juillet) \
Laitues pommée Brune Percheronne : [https://kokopelli-semences.fr/fr/p/P3829-Brune-Percheronne](https://kokopelli-semences.fr/fr/p/P3829-Brune-Percheronne)&#x20;

### **Laitues pour l'automne :**&#x20;

Laitue à couper Flame : [https://kokopelli-semences.fr/fr/p/P4162-Flame](https://kokopelli-semences.fr/fr/p/P4162-Flame)\
Laitue pommée Carmona : [https://kokopelli-semences.fr/fr/p/P3833-Carmona](https://kokopelli-semences.fr/fr/p/P3833-Carmona) \
Laitue pommée Palatine : [https://kokopelli-semences.fr/fr/p/P3830-Palatine](https://kokopelli-semences.fr/fr/p/P3830-Palatine)&#x20;

### **Laitues pour l'hiver (pas dans la rotation, mais pourquoi pas) :**&#x20;

Laitue pommée Merveille d'hiver : [https://kokopelli-semences.fr/fr/p/P3903-Merveille-d-Hiver](https://kokopelli-semences.fr/fr/p/P3903-Merveille-d-Hiver) Laitue pommée Rouge d'Hiver : [https://kokopelli-semences.fr/fr/p/P3907-Rouge-d-Hiver](https://kokopelli-semences.fr/fr/p/P3907-Rouge-d-Hiver)

### Engrais verts associés

Pas besoin d'engrais vert spécifiques, car les laitues sont peu gourmandes en nutriments.&#x20;

### Plantes compagnons

[thym.md](../plantes-compagnon/vivaces/thym.md "mention")

Fonctionne bien avec les pommes de terre, tomates, choux et légumineuses. Pour éloigner les limaces, il est possible de planter à côté des liliacés, du thym ou des capucines.&#x20;

### Nombre de graines/poids nécessaire pour le printemps

Dans un sachet de 1g il y a environ 800 graines. Si on vise de planter 2 fois plus pour éviter de trop lourde perte avec les limaces et qu'une graine sur trois donne, alors environ 1g donne 100 salades produites (avec une vision pessimiste). Si on vise un rendement optimiste de 10 laitues par mètre carré (comme mis dans la planif) alors il faut 1 sachet par planche (car planche de 10,5 mètre carré). Donc prévoir d'acheter au total pour le printemps au minimum 10 sachets de graines car 10 planches sont prévues (ça fait beaucoup non ?). Donc 3 pour chaque variétés de printemps semble OK (cf au-dessus). Ca va en faire des semis à prévoir en intérieur ! ## Autres notes ## Pour résister au nombreux assauts des limaces il est recommandé de : - pailler - durant les semis couper quelques feuilles pour simuler des attaques afin de les renforcer - planter plus de laitues que nécessaires - planter plutôt des laitues foncées (et non vertes)
