---
description: >-
  Agriculture périurbaine collective : circuit parllèle, résilience et
  efficacité.
---

# Jardins collectifs russes

Occupation temporaire des jardins (de 600m²) pendant la saison productive, dans des maisons en dur le plus souvent, surtout aux alentours des grosses villes.

### Critères de résilience des systèmes alimentaires :&#x20;

[https://resiliencealimentaire.org/](https://resiliencealimentaire.org/) (Les Greniers d'Abondance)

Les collectifs de jardins russes cochent beaucoup de ces critères.

\-- Diversité : diversification du régime alimentaire (produits frais pas ou peu disponibles ailleurs, etc). Diversification des cultures avec quelques invariants : pdt, poivron, tomate, concombre. Arbres fruitiers, de nombreuses variétés pour chaque culture, fleurs annuelles. Biodiversité utile contre les maladies, les ravageurs, et la mauvaise météo.&#x20;

\-- Modularité : indépendance de fonctionnement, taille modeste des parcelles. Séparation avec les filières agro-industrielles et les circuits de vente professionnels : troc et auto-consommation seulement. Pas de dépendances aux lois du marché, ni aux infrastructures industrielles pour la production, la distribution, la commercialisation.&#x20;

\-- Cyclicité : Pas d'intrants chimique, mais compost et fumier local. Toilettes sèches ou fosses sceptiques. Constructions démontables en matériaux de récupération (surtout du bois) qui engendrent peu de déchets, chemins d'accès en terre pour ne pas imperméabiliser les sols.&#x20;

\-- Ancrage local : Proximité entre les habitations et les jardins, à pieds ou en transports en commun. La plupart arrivent à nourrir leur famille grâce au jardin pendant la période estivale, voire pendant toute l'année pour certaines cultures comme la patate.&#x20;

\-- Implication collective : visée militante, politique, pragmatique : échapper aux famines et disettes, trouver de l'indépendance alimentaire. Résilience psychologique face au stress induit par les situations de conflits, responsabilisation par rapport à la contribution collective à la souveraineté alimentaire.&#x20;
