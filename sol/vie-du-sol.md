---
description: >-
  La vie d'un sol est une nécessité pour que les plantes poussent et que les
  cultures soient abondantes. Les interactions de la vie dans le sol sont très
  complexes.
---

# Vie du sol

Les macro- et micro-organismes présents dans la terre, ainsi que les intempéries, dégradent la matière organique (MO) (feuilles, branches, fruits, etc) et la transforment en humus (qui retient les éléments minéraux et l'eau) : c'est l'humification. Ces organismes digèrent ensuite l'humus et le transforment en forme minérale pour rendre assimilable par les plantes les éléments nutritifs : c'est la minéralisation. L'humification et la minéralisation se déroulent dans la fraction humique, c'est-à-dire la couche superficielle du sol (4 à 6cm). Ce système est excédentaire grâce à la production de biomasse par la photosynthèse, qui est stockée sous forme d'humus dans le sol, qui est nécessaire à la vie du sol pour se nourrir et avoir un environnement viable.

### _Invertébrés_

#### Macrofaune (de 4 à 80mm)

vers de terre, insectes (fourmis, termites, larves, etc), arachnides, mollusques (ecargots, limaces, etc), crustacées (cloportes, etc), myriapodes, etc.

#### Mésofaune (0,2 à 4mm)

acariens, petits insectes, petits vers, etc.

#### Microfaune (<0,2mm)

amibes, etc

### _Microflore_

Bactéries, virus, champignons, micro-algues, etc.
