---
description: >-
  La porosité d'un sol décrit les trous qu'il y a dans le sol, et qui peuvent
  être remplis par de l'eau ou de l'air.
---

# Porosité

Ces trous peuvent être des galeries, l'espace occuppé par d'anciennes racines décomposées, des micropores, etc.&#x20;

La porosité d'un sol est directement reliée à la capacité de rétention de l'eau de ce sol. Un sol poreux amènera l'eau de pluie en profondeur, ainsi que l'air (le dioxygène et l'azote surtout). La porosité détermine donc l'eau disponible pour les plantes : dans l'idéal, ce doit être ni trop ni trop peu.&#x20;

Le contraire d'un sol poreux est un sol compact, qui empêche l'eau de pénétrer jusque dans les nappes phréatiques. Il s'érode lors d'épisodes de fortes pluies. L'érosion amène les polluants et la terre vers les points bas, qui sont souvent des points de prélèvement de l'eau potable.&#x20;

