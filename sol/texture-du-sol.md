---
description: >-
  La texture du sol (sableux, limoneux, argileux, calcaire ou humifère) décrit
  de quoi est composé le sol.
---

# Texture

On connait la _**texture du sol**_ grâce aux proportions de sable, de limon, d'argile, de calcaire et d'humus du sol.&#x20;

On peut la déterminer en décantant la terre mélangée à de l'eau, et en observant les proportions à l'aide du triangle des textures de terre.&#x20;

Une deuxième technique, moins précise mais plus rapide, consiste à essayer de faire un boudin de 5mm de diamètre avec la terre : si faire le boudin est impossible, la sol est sableux. Si on peut faire un boudin mais qu'il ne se tient pas, le sol est limoneux. Si on peut faire un boudin solide, le sol est argileux.

Un _**sol argileux**_ retient l'eau et les sels minéraux, mais il est peu perméable à l'air et à l'eau. Il devient dur par temps sec, et laborieux à travailler par temps humide. C'est un sol fertile mais compliqué à travailler, il faut éventuellement l'amender pour améliorer sa texture et la rendre moins lourde. On peut par exemple lui ajouter de la perlite, du sable, des gravillons, en plus de passer la grelinette avant chaque semis, de faire des buttes et d'ajouter de la matière organique. &#x20;

Un _**sol limoneux**_ est facile à travailler et a un bon équilibre. Il faut l'arroser abondamment mais pas trop fréquemment. Une croute de battance peut se former à la surface par temps de pluie. Il a une bonne rétention en eau. Il faut un peu le nourrir en matière organique, mais c'est un sol qui réunit les qualités.&#x20;

Un _**sol sableux**_ se réchauffe rapidement en hiver et au printemps, c'est une terre légère et facile à travailler. Il est perméable à l'eau et à l'air. L'eau et les sels minéraux se drainent plus facilement dans ce sol. Il faut donc éventuellement l'amender pour ajouter de la matière organique, le couvrir d'engrais verts, et l'arroser régulièrement.

Une certaine texture du sol est meilleure pour telle ou telle culture : par exemple, les carottes se plaisent mieux dans les sols sableux. Mais il est possible de faire pousser des plantes partout avec une bonne nutrition et une bonne irrigation, c'est principalemet l'activité biologique des sols qui fait sa fertilité. Malgré ça, il peut être difficile de cultiver dans des argiles purs, des sables purs, ou dans des sols très caillouteux.&#x20;
