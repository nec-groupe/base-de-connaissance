---
description: Le ph mesure l'acidité ou la basicité du sol.
---

# pH

pH < 6 : sol acide. Souvent les sols sableux. Nuit à l'activité biologique.&#x20;

6\<pH<7 : le pH est optimal pour un sol, l'idéal étant à 6,5.&#x20;

pH > 7,5 : sol basique. Souvent les sols calcaires.&#x20;

Un Ph idéal pour un sol est compris entre 6,5 et 7.&#x20;

Les matières organiques de conifères acidifient le sol (font baisser le pH), tandis que les cendres de bois ou la chaux augmentent le pH.&#x20;

Si on veut mettre de la chaux ((inconvénients de la chaux à creuser), il faut l'incorporer graduellement et à petites doses au sol, sur les 15 premiers cm du sol.&#x20;

