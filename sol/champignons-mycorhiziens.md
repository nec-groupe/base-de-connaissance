---
description: >-
  Les champignons mycorhiziens sont des champignons qui fonctionnent en symbiose
  (relation/collaboration gagnant-gagnant) avec les racines des plantes.
---

# Champignons mycorhiziens

La surface des champignons est hygroscopique, ce qui veut dire qu'elle attire et retient l'eau, et peut la faire voyager le long de ses racines. L'eau peut ainsi voyager des nappes phréatiques jusqu'à la surface, jusqu'aux plantes liées aux champignons mycorhiziens, mais aussi celles qui n'y sont pas liées. C'est la raison pour laquelle la capacité d'un sol à retenir l'eau est liée à la quantité de champignons mycorhiziens, et donc d'arbres, qui le composent.

Les champignons mycorhiziens permettent aussi aux plantes d'explorer le sol et de se nourrir, car cela leur demande moins d'énergie qu'avec leurs propres racines. Cependant, cette faculté des cultures a peu à peu disparu à cause de la séléction génétiques et des engrais de synthèse, car elles n'en avaient plus besoin.&#x20;

#### Exemple d'une symbiose entre champignon et lierre :&#x20;

L'eau se condense sur le lierre, et la sève élaborée du lierre se dilue. Les champignons mycorhiziens maintiennent la pression de l'eau dans les racines, et par conséquent sort l'eau pour que la pression reste la même dans toute la plante. L'eau se retrouve donc dans le sol. Par ailleurs, c'est le même mécanisme, la pression dans les vaisseaux conducteurs de la plante, qui permet aux plantes d'aspirer l'eau du sol après l'évapotranspiration, comme dans une paille. Ces vaisseaux conducteurs, qui acheminent l'eau à travers la plante, sont plus ou moins gros en fonction des besoins et de l'environnement de la plante.

Si il y a un réseau de bois dans le sol (coupe de bois, taille des branches, [trognes](../plantes/arbres/trognes.md), etc), l'eau est transportée et répartie autour de l'arbre par le bois en décompostion dans le sol et par les champignons mycorhiziens liés à cet arbre, et ce jusqu'à 2,5 fois la hauteur de l'arbre.

#### Trames endomycorhiziennes et ectomycorhiziennes

"Endo" veut dire "à l'intérieur", tandis que "ecto" veut dire "à l'extérieur". Les termes "endomycorhizien" et "ectomycorhizien" (endo- et ecto- ici) définissent deux types de trames mycorhiziennes : dans l'une, les champignons mycorhiziens pénètrent les racines de l'intérieur pour que la symbiose fonctionne, et dans l'autre, les champignons entourent seulement les racines. Ces deux trames sont relativement indépendantes, et ne travaillent pas ou peu ensemble. Nos plantes cultivées ont besoin d'une trame endo-, tandis que la trame de nos forêts est en majorité ecto-.&#x20;

Certains arbres, comme les arbres fruitiers ou les arbres et arbustes fixateurs d'azote, vont favoriser la trame endo-. La ronce, le houblon, la vigne, l'arbousier, etc, peuvent se connecter aux deux trames endo- et ecto-, en transformant les trames ecto- en trames endo-, ce qui est très utile pour les cultures.&#x20;

Les champignons ecto- protègent les racines des pathogènes par leur manchon mycellien (ensemble de filaments des champignons, pour le dire simplement), qui enveloppe les racines.&#x20;

Avec cette façon de faire, les forêts ecto- disparaissent et une nouvelle forêt endo- peut émerger, et peut donc travailler de manière optimale avec les cultures : des arbres fruitiers, des arbres forestiers endo-, des jardins forestiers (voir [forêt-jardin](../techniques-de-cultures/foret-jardin.md)), des vergers endo-, des céréales, etc.

Cette forêt et cet écosystème va réagir, s'adapter et s'amplifier face au réchauffement climatique. L'idéal est de mettre en place une trame mixte ecto- et endo- pour une bonne compatibilité entre les plantes cultivées et le milieu forestier. C'est pour cette raison qu'il faut planter des arbres aux fonctions multiples : arbres à noix, arbres fruitiers, arbres sauvages, etc.&#x20;



Source principale : youtube, Les racines de la souveraineté, E04 et E07

