---
description: >-
  Il est très important de comprendre l'interaction plante-sol et de savoir
  comment bien intervenir pour favoriser cette relation.
---

# 🪱 Sol

Il faut trouver l'équilibre entre trois facteurs : les rendements, le maintien de le fertilité à long terme, et l'efficacité des opérations.&#x20;

### Travail du sol

Pour plusieurs raisons, le travail du sol doit se réduire au minimum pour conserver l'équilibre du sol, maintenir la vie à l'intérieur, et ainsi assurer la bonne croissance des plantes. Le bêchage ou le labour (qui consistent à retourner la terre sur quelques cm ou quelques dizaines de cm) est un non-sens biologique à moyen et long terme : on enfouit ainsi en profondeur les parties supérieures du sol riches en matière organique et en organismes vivants, et on ramène en surface les parties inférieures du sol. Les organismes vivants ne sont pas à leur place : ceux qui ont beoin de lumière ou d'air, et qui étaient donc en surface, se retrouvent en profondeur, et inversement, ceux qui avaient besoin d'obscurité ou des éléments minéraux en profondeur, se retrouvent en surface. On perturbe ainsi l'équilibre du sol, les animaux meurent, et on retarde le bon fonctionnement du sol.

De plus, le fait d'amener de l'air dans les profondeurs du sol amène des bactéries qui minéralisent l'humus stable du sol, et le transforment en CO2, et réduisent ainsi la quantité d'humus dans le sol. &#x20;

Les bénéfices du labour à court terme (cassage des mottes, enfouissement des mauvaises herbes et remontée des éléments nutritifs pour les cultures) ne suffisent pas à le justifier dans une démarche de se réapproprier la culture sur le long terme.

Le travail du sol doit se réduire au minimum donc, par exemple lorsqu'il est très compact et sans [porosité](porosite.md). Dans ce cas, il va falloir nourrir davantage le sol pour contrebalancer la minéralisation qui fait baisser la fertilité.

On peut cependant l'aérer pour préparer des [planches permanentes](../methodes/preparation-dune-planche.md) ou pour préparer des transplantations ou des semis directs. Dans ce cas, il faut des outils appropriés pour ne pas endommager la structure du sol et la vie du sol.&#x20;

### Complexe argilo-humique

Le complexe argilo-humique est la partie fertile du sol. D'une part, il est composé de la dégradation de la roche-mère (la roche située sous le sol) par les plantes, les micro-organismes et le climat, et libère des sels minéraux et de l'argile. D'autre part, il est composé de matière organique végétale et animale restituée sous forme d'humus au sol (voir [Vie du sol](vie-du-sol.md)). Ces deux parties se mélangent et se combinent pour former le sol.

