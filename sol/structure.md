---
description: >-
  Manière dont sont arrangés les différents composants du sol, liens entre les
  éléments minéraux et organiques (tassés, soudés, etc).
---

# Structure

La structure du sol détermine la circulation de l'air et de l'eau en profondeur. La structure dépend de l'argile et de l'humus (le complexe argilo-humique).&#x20;

Un sol instable se tient mal, se compacte et perd facilement ses minéraux, alors qu'un sol stable résiste à la compaction, reste aéré et retient mieux les sels minéraux. Il y a trois types de structure : particulaire/élémentaire (les constituants solides sont entassés sans aucune liaison), compactes/continues (les constituants ne forment qu'un seul bloc, le sol est compact et imperméable à l'eau et à l'air), fragmentaires (agrégats, structure grumeleuse formée par la faune du sol). Cette dernière est la structure qu'il faut chercher, car elle permet une bonne pénétration de l'eau et de l'air dans le sol.&#x20;

Les vers de terre participent à la structure du sol, comme les bactéries, qui asssemblent les particules du sol grâce à leur biofilm. Certains champignons font la même chose, ce qui favorise la croissance des plantes, le couvert végétal, et améliore la santé du sol.&#x20;
