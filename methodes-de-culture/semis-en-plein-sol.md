---
description: Mettre directement la graine dans le sol, sans faire de semis avant ça.
---

# Semis en plein sol

Les graines poussent plus rapidement, les cultures sont plus faciles d'entretien et moins chères que les semis intérieurs, mais la densité est plus difficile à contrôler, et il y a plus de désherbage.

Un semoir peut être utile pour réussir à semer à espacements réguliers, et pour ne pas avoir à éclaircir les planches ensuite. Détails dans [le jardinier-maraîcher](../bibliotheque/le-jardinier-maraicher.md) sur les modèles et leurs avantages et utilisations.&#x20;

Un bon taux de germination est important, il faut des graines de qualité. Éviter de garder les graines de l'année précédente, et les garder dans un endroit frais, sec et hermétique.&#x20;

Le sol devrait toujours être gardé humide jusqu'à l'émergence des semis. Calculer la densité de graines idéale, et semer en fonction. Mieux vaut semer plus que pas assez, même s'il faut éclaircir ensuite.&#x20;

Prendre des notes de ce qui est semé, à quel endroit, quand, quel espacement, etc.

### Arbres et arbustes

Pour les arbres et arbustes, le semis a beaucoup d'avantages sur les plants de pépinières : les racines pivots sont conservées et permettent aux plantes de s'enraciner plus profondément, et ainsi de chercher l'eau et les nutriments plus efficacement, et de résister aux vents. Les graines coutent moins cher que les plants. Cependant, elles sont plus lentes à donner des fruits que les plants de pépinières ou issus de greffes.

#### Semis d'arbres :&#x20;

Remplir une cagette aux bords fermés d'une mélange 50% sable silicieux et 50% compost de feuilles de chêne. PLacer les noyaux dessus à plat, puis recouvrir de 7cm de mélange, puis arroser. Placer ensuite la cagette dans un endroit froid du jardin pendant un certain temps, se renseigner sur la durée de stratification en fonction de l'espèce. À la fin de la stratification et à la fin de l'hiver, exposer la cagette au soleil pour provoquer la germination.&#x20;

Quand le noyau a germé, on peut planter : trou en forme de carotte, rempli avec de la bonne terre tamisée et du compost, sur lequel on pose le noyau, et qu'on recouvre de 5cm de mélange. Poser un grillage de protection contre les prédateurs.&#x20;

