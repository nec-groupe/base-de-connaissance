---
description: >-
  Semer les graines dans des pots, attendre qu'elles poussent un peu à
  l'intérieur, puis les transplanter en terre plus tard.
---

# Semis intérieurs

Pépinière pour produire les transplants de légumes. Cela nécessite une attention au détail et du savoir-faire, surtout pour la production horticole en serre, c'est une science poussée mais on peut y arriver sans pour autant en faire une spécialité.&#x20;

#### Avantages :&#x20;

saisonnalité des légumes plus longue (démarrage de la production plusieurs semaines avant les derniers gels), contrôle des conditions de germination et de croissance en début de culture (alors que les plants sont encore fragiles), densité parfaite des semis, les cultures devancent les mauvaises herbes, possibilité de démarrer la culture avant que l'espace soit disponible au jardin.&#x20;

#### Conditions :&#x20;

Dans une maison : température de 18°C à 20°C, avec le plus de luminosité possible (fenêtres au sud). Possibilité aussi de les mettre dans une véranda ou une serre, prévoir une aération et un voilage léger dans ce cas pour les journées chaudes et ensoleillées.





Détails sur les semis en multicellules, le terreau, la préparation des semis, les conditions de température, d'humidité, de lumière, de ventilation pour que les semis poussent bien, l'arrosage, sur le chauffage et les fournaises, sur les pépinières professionnelles, le repiquage, la transplantation : voir [le jardinier-maraîcher](../bibliotheque/le-jardinier-maraicher.md), page 93.
