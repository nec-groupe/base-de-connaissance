---
description: .
---

# Climats

Cartes agroclimatiques, carte "unité thermique maïs" (UTM),  carte des zones de rusticité, carte de zonage bioclimatique. ( -> voir [le jardinier-maraîcher](../bibliotheque/le-jardinier-maraicher.md))

Climat méditerranéen : climat chaud et souvent sec. Il faut beaucoup irriguer les cultures. Attention à l'érosion des sols dûe aux pluie denses. Cultiver sous les arbres, apporter de l'humus, ainsi que pailler les sols, est presque une nécessité.&#x20;
