---
description: >-
  La biodiversité est la diversité de l'ensemble des espèces vivantes dans un
  milieu donné, et leurs interactions entre elles.
---

# Biodiversité

Dans un milieu biodiversifié, il y a un système de paliers, ou de points de non-retour : certaines espèces sont nécessaire au système entier, et leur disparition ou leur raréfaction met en danger l'ensemble des autres espèces. Par conséquent, les changements irréversibles ou drastiques peuvent subvenir rapidement, et sans forcément de signes avant-coureurs notables.&#x20;

Compte tenu de la complexité d'un système où la biodiversité est conséquente, c'est très dificille à modéliser, et donc à prévoir.

La fertilité des sols est très liée à la biodiversité.
