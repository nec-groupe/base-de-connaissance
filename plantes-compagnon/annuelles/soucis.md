---
description: .
---

# Soucis

<figure><img src="../../.gitbook/assets/soucis.jpg" alt=""><figcaption></figcaption></figure>

Les soucis sont des plantes de la Famille des _Asteraceae_ et de la Tribu des _Calenduleae_. Le genre _Calendula_ comprend 20 espèces connues. Elles sont toutes médicinales, mellifères et tinctoriales. Le souci offre une couleur jaune crème, obtenue par décoction de ses fleurs et fournit une source de colorant alimentaire non toxique, notamment utilisé pour foncer les beurres. Reconnu pour ses vertus calmantes et cicatrisantes en usage externe, le souci officinal - _Calendula officinalis_ - soulage, en usage interne, les troubles gastriques, les affections hépatiques, les inflammations de la gorge et de la bouche ou encore les douleurs prémenstruelles.

## Variétés

Chez [kokopelli.md](../../semenciers/kokopelli.md "mention"), les graines sont vendues par **sachet de 2 g.** Un sachet peut couvrir **8 m².** \
\
Avec nos [planche.md](../../organisation-des-cultures/organisation-spatiale/planche.md "mention")s, il nous faut donc **un sachet par planche**.

{% embed url="https://kokopelli-semences.fr/fr/c/semences/plantes-diverses/calendulas" %}

{% embed url="https://kokopelli-semences.fr/fr/p/E0133-Flashback-Mix" %}

## Protège

[melon.md](../../plantes/melon.md "mention")

