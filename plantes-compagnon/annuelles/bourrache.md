---
description: .
---

# Bourrache

La bourrache est mellifère. Cela signifie qu’elle produit une **importante quantité de nectar** et de pollen d’excellente qualité et dont les abeilles et les papillons raffolent. C’est donc un véritable atout pour un jardin dans lequel on veut **attirer les insectes pollinisateurs**.

Elle a d’ailleurs d’autres avantages au jardin puisqu’elle se ressème sans aide extérieure, elle est **riche en minéraux** notamment en calcium et en potassium et elle **repousse les limaces et les chenilles**. C’est alors une alliée précieuse pour les jardiniers et leurs plantations.

<figure><img src="https://www.autourdupotager.com/wp-content/uploads/2021/04/poils-bourrache.jpg" alt=""><figcaption></figcaption></figure>

Les feuilles de bourrache fraîche sont une excellente **source de vitamine C**, on peut ainsi en mettre dans de nombreuses préparations culinaires.

On peut les garder crues pour **les ajouter à une salade** ou les cuisiner en les incorporant à une soupe par exemple ou dans un cake. Elles peuvent aisément remplacer [les épinards](https://www.autourdupotager.com/epinard/) ou les blettes dans une tarte, car elles se cuisinent de la même façon.



{% embed url="https://kokopelli-semences.fr/fr/c/semences/plantes-diverses/bourraches" %}

## Protège

[pomme-de-terre.md](../../plantes/pomme-de-terre.md "mention")

## Repousse

**limaces et chenilles**

## Sources

{% embed url="https://www.autourdupotager.com/bourrache/" %}

