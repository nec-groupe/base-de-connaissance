---
description: .
---

# Coriandres

La coriandre — _Coriandrum sativum_ — est une plante de la Famille des _Apiaceae_ cultivée depuis l'Antiquité pour ses vertus médicinales, mais également pour sa saveur si particulière. Connue partout sur la planète, cette aromatique présente la qualité d'être digestive, antioxydante, antibactérienne, anxiolytique, mais aussi chélatrice. C'est son pouvoir de [chélation](https://fr.wikipedia.org/wiki/Ch%C3%A9lation) qui lui permet de désintoxiquer l'organisme des isotopes radioactifs et des métaux nocifs - plomb, mercure, etc. Elle s'avère donc excellente à consommer en association à du kéfir, de la chlorella - attention à sa provenance - ou de la zéolite. Le genre _Coriandrum_ comprend 2 espèces connues. On utilise ses feuilles et ses graines.\
\
Originaire du sud-ouest de l’Europe et du Moyen-Orient, la coriandre était déjà connue des Égyptiens, des Grecs et des Romains qui utilisaient ses graines pour aromatiser leur pain. Au Moyen Âge, elle fut répandue dans toute l’Europe pour son usage culinaire et reconnue pour ses vertus antibactériennes et antifongiques. Tant ses feuilles que ses graines renferment des antioxydants. Les feuilles de coriandre sont une très bonne source de vitamine K, nécessaire à la coagulation sanguine. Les graines de coriandre soulagent les affections gastro-intestinales, l’aérophagie et les ballonnements. Elles sont efficaces en cas de troubles de la digestion. Stimulante, la coriandre accroît le tonus de l’organisme et permet de lutter contre la fatigue et les états grippaux. La coriandre est parfois recommandée contre l’anxiété et pour favoriser le sommeil.

{% embed url="https://kokopelli-semences.fr/fr/c/semences/plantes-diverses/coriandres" %}

{% embed url="https://kokopelli-semences.fr/fr/p/C0167-Coriandre" %}

## Protège&#x20;

[pomme-de-terre.md](../../plantes/pomme-de-terre.md "mention")
