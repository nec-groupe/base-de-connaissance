---
description: .
---

# Aneth

l**’aneth** protège les concombres et carottes ;



## Variétés&#x20;



{% embed url="https://kokopelli-semences.fr/fr/c/semences/plantes-diverses/aneths" %}

{% embed url="https://kokopelli-semences.fr/fr/p/A0156-Bouquet" %}

Sachet de 200 graines, 100 graines par mètre = 5 sachets pour une planche complete, à voir en fonction des autres plantes sur la même planche.



