---
description: .
---

# Capucines

Les capucines — _Tropaelum majus_ — sont des plantes herbacées médicinales de la Famille des _Tropaeolaceae_. Le genre _Tropaeolum_ comprend plus de 85 espèces que l’on retrouve à l’état sauvage du sud du Mexique jusqu'en Patagonie. Le terme _Tropaeolum_, du grec tropaion « trophée » fait allusion à la forme de ses feuilles et de ses fleurs qui ressemblent aux boucliers et aux casques des soldats de l’Antiquité. On utilise les fleurs et les feuilles crues pour assaisonner les salades. Ses boutons floraux se préparent comme des câpres au vinaigre et la racine peut être consommée cuite. Cette plante médicinale est aussi appréciée pour ses bienfaits sur le système immunitaire, les problèmes respiratoires, la cicatrisation, la chute des cheveux et le système urinaire.

{% embed url="https://kokopelli-semences.fr/fr/c/semences/plantes-diverses/capucines" %}

## Protège

[laitues.md](../../plantes/laitues.md "mention")\
Choux

