---
description: >-
  Une plante compagnon est un plante qui va aider une culture à mieux pousser,
  pour différentes raisons.
---

# 🌱Plantes compagnon

Une plante compagnon peut repousser les ravageurs auxquels la culture est sensible, attirer ses prédateurs, nourrir le ravageur pour éviter qu'il mange la culture, faire un symbiose avec la culture (échange de nutriments, etc), ou simplement avoir des racines de profondeurs différentes ou prendre des nutriments différents dans le sol pour ne pas l'épuiser.&#x20;

Ce peut être des fleurs, d'autres légumes, des plantes diverses et variées.
