---
description: C'est de l'herbe à chat !
---

# Nepeta

{% embed url="https://kokopelli-semences.fr/fr/p/E0118-Nepeta-tuberosa" %}

## Protège

[pomme-de-terre.md](../../plantes/pomme-de-terre.md "mention")

