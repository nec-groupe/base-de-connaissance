---
description: et parasites, etc
---

# 🐛 Maladies et ravageurs

Les maladies diverses et variées sont un symptome qui montre que la plante est faible, mal nourrie,  ou au mauvais endroit, dans le mauvais sol, etc. Si une plante est nourrie parfaitement par le biais d'un sol équilibré, celle-ci ne devrait pas être sujette aux maladies ou aux insectes ravageurs.&#x20;

Lorsqu'on a un problème avec un ravageur ou une maladie, il faut définir la place du problème dans le cycle biologique, comprendre les mécanismes qui l'entourent, comprendre sa fonction.



Piège à nématodes pour lutter contre les escargots et les cloportes qui mangent les bulbes de gingembre.



En plus de pouvoir prévenir des maladies et ravageurs, passer du temps au jardin permet d'observer la vie du jardin, et de se rendre compte de ce qui s'y trouve : cultures, animaux, plantes, etc. Cela affine le regard et permet d'en apprendre beaucoup sur la biologie des plantes.&#x20;
