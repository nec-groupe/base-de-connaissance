---
description: .
---

# Limaces

Habitat naturel : litière de la forêt.&#x20;

Elles mangent les jeunes pousses de plantes, de salades surtout. Elles se nourrissent de lignine, de champignons, et ont besoin d'acides aminés.&#x20;

Cependant, elles peuvent être utiles aux cultures, car si elles ont assez à manger ailleurs que dans les cultures, elles mangent les jeunes pousses malades seulement.

#### Solution sur le long terme :&#x20;

Planter des capucines pour les éloigner. Ca peut se semer en pleine terre à partir de mi-Avril. Planter du thym pour les éloigner. Il est préférable de faire des semis à l'abri surtout en Mars pour pouvoir replanter en Avril-Mai. Mettre en place d'autres cultures "paratonnerres" à sacrifier pour attirer les limaces dessus. Trouver des solutions pour attirer leurs prédateurs : hérissons, crapauds, musaraignes et certains oiseaux.

#### Solutions sur le court terme :&#x20;

Barrer la route aux limaces soit avec du marc de café, de la cendre, des coquilles d'oeuf écrasées, des pierres de lave, ... Utiliser des planches pour retirer les limaces. La journée, les limaces vont se placer en dessous des planches, il est donc facilement possible de les retirer manuellement (pratique pour un potager proche de chez soi mais pas pour le champ).
