---
description: >-
  Petit guide des plantes comestibles - 70 espèces a découvrir. Autrice :
  Morgane Peyrot, 2019, 206 pages
---

# Petit guide des plantes comestibles

{% file src="../.gitbook/assets/Morgane Peyrot - Petit guide des plantes comestibles - 70 especes a decouvrir.pdf" %}
