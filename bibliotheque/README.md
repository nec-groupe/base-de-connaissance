---
description: .
---

# 📚 Bibliothèque

Ici se trouvent des références de livres, ainsi que des informations à leurs sujets (résumés, sommaires, contexte, etc). Les livres sont dispos dans la bibliothèque, demandez nous sur discord.

## Liste des livres disponibles :&#x20;

Guide Maraîchage Sol Vivant 2022

Le jardinier-maraîcher, Jean-Martin Fortier

Le petit guide du compost, Saine abondance
