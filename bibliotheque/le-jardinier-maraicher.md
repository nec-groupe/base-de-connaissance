---
description: >-
  Auteur : Jean-Martin Fortier, édité en 2012, revu et augmenté en 2015, 242
  pages
---

# Le jardinier-maraîcher

### Présentation :&#x20;

Jean-Martin Fortier et sa compagne vivent d'une petite exploitation d'environ un hectare au Québec, en agriculture biologique intensive. Leur situation est donc bien différente de la nôtre à Mauves, mais le livre offre cependant beaucoup de pistes pour produire efficacement sur une petite surface, et aborde beaucoup de sujets incontournables avec précision. On s'en est beaucoup inspiré jusqu'à présent, particulièrement sur la standardisation de l'espace et sur la rotation des cultures.

### Sommaire :

Introduction 8

Small is beautiful (petites surfaces, aspect commercial) 17

Réussir un jardin maraîcher (cultures intensives, investissements, coûts, etc) 21

Trouver un bon site (comment choisir en fonction de l'eau, du climat, de la surface, des marchés, des polluants, du sol, etc) 33

Établir ses jardins (organisation, protection, eau, standardisation, etc) 47

Le travail minime du sol et la machinerie alternative 56

La fertilisation organique (chimie du sol, compost, vie du sol, rotation des cultures, engrais verts, etc) 66

Les semis intérieurs (multicellules, pépinière, chauffage des serres, transplantation, etc) 93

Les semis en plein sol 108

Le désherbage (outils, faux-semis, paillis, approche préventive, etc) 116

Les insectes nuisibles et les maladies (dépistage, prévention, biopesticides) 126

Le prolongement de la saison (différents types de tunnels) 133

La récolte et l’entreposage 140

La planification de la production 146

Conclusion 155

Annexes (notes culturales sur différents légumes, fournisseurs de matériel, exemple de plan de jardin, glossaire et bibliographie) 158
