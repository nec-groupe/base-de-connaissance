---
description: 'Auteur(e)s : J.G. Moreau et J.J. Daverne, 1845, 221 pages'
---

# Manuel pratique de la culture maraichere de Paris

{% file src="../.gitbook/assets/Moreau-Daverne - Manuel pratique de la culture maraichere de Paris.pdf" %}
