---
description: édité par Saine Abondance, relativement récemment. 65 pages
---

# Le petit guide du compost

{% file src="../.gitbook/assets/Le petit guide du compost, Saine Abondance.pdf" %}

### Résumé : &#x20;

Le sol est un milieu organo-minéral, ce qui veut dire qu’il est composé d’une partie minérale et d’une partie organique.

**Partie minérale** : altération du soubassement rocheux. Les sels minéraux des roches sont solubilisées dans l’eau pour pouvoir être absorbées par les plantes. La fertilité minérale d’un sol dépend de ces sels minéraux, et donc de la nature de la roche-mère ainsi que de son degré d’altération.

**Partie organique** : matière organique produite par tous les êtres vivants qui s’y trouvent (animaux dans le sol, sur le sol, végétaux, etc.) La décomposition se fait en 2 étapes : l’humification (la matière organique (MO) est transformée en humus), puis la minéralisation, où a lieu la libération des sels minéraux.

Les interactions entre cette partie oragnique et cette partie minérale forment le **complexe argilo-humique**, qui est un réservoir d’éléments nutritifs, et qui fait le lien entre les particules du sol pour permettre à l’air, l’eau et aux racines des plantes de pénétrer la terre en profondeur, ce qui est une bonne chose pour la croissance des plantes.

D’autres mécanismes apportent des éléments minéraux au sol (fixation de l’azote de l’air par certaines plantes, apports extérieurs) ou lui en font perdre (drainage, dénitrification, cristallisation, absorption par les végétaux).

Pour conserver la qualité biologique et donc nutritive (car les deux sont intimement liées) d’un sol cultivé, il faut apporter de la matière minérale et de la MO au sol. Le compostage permet justement cela, car il transforme de la MO fraiche en MO stable, qui améliore la fertilité physique (eau, aération) et chimique (rétention des sels minéraux, des nutriments, etc) du sol, à travers les complexes argilo-humiques. Le compost est donc à la fois un amendement, puisqu’il améliore la structure du sol, et un engrais, puisqu’il lui fournit des nutriments.

Différents animaux décomposent la MO et sont nécessaires aux compost : lombrics, millepattes, insectes, acariens, champignons, bactéries, etc. Ils ont besoin d’un compost aéré pour avoir accès à l’oxygène de l’air.

Dans un jardin, un compost doit être mature, sans quoi il nuit aux jeunes plantes cultivées à cause de certains des composés de ce compost, qui empêchent la germination. Il faut mélanger le compost mûr à la terre sur quelques centimètres de profondeur, à l’automne, en fin d’hiver ou au printemps. Un sol limoneux ou sableux aura un besoin en compost plus grand qu’un sol argileux ou calcaire, qui retiendront mieux l’humus.

Il y a deux types de composteurs : le composteur en tas et le composteur en silo.

**Composteur en tas** : Il est humidifié par la pluie, il faut le couvrir avec une bâche s’il pleut trop. Son aération est importante. Le compost murit en 12 à 18 mois. Le tas doit faire environ 1,5m de diamètre et peut aller jusqu’à 1,5m de haut.

**Composteur en silo** : Le compost murit en 6 à 12 mois. Il faut brasser sur 20cm à chaque apport hebdomadaire de déchets frais, puis recouvrir de matière sèche. Il y a 2 bacs : un pour les déchets frais, l’autre pour la maturation du compost.

Quelques autres bonnes attitudes à avoir pour nourrir son compost et pour l’utiliser : \
\-  Installer le composteur à l’abri du vent et du trop-plein de soleil \
\-  Couper les déchets frais en petits morceaux avant de les composter \
\-  Éviter les graines présentes dans les déchets ou le compost, qui peuvent repartir plus tard dans les jardins, et ainsi faire des mauvaises herbes \
\-  Éviter d’apporter du sel au compost (eau de cuisson salée par exemple) \
\-  Éviter de mettre des plantes malades dedans, il vaut mieux les jeter à la poubelle ou loin ailleurs \
\-  Éviter les produits chimiques (colle, bois traité, encre, etc) \
\-  Mettre de la matière sèche et du carton brun sur des déchets sucrés, comme les fruits, pour ne pas attirer les moucherons \
\-  Tamiser le compost mûr avant son application au jardin (trous de 1-2cm ?) et remettre les composés les plus gros au compost

Il faut également respecter un **équilibre entre matière sèche** (MS, aussi appellée matière brune) qui est riche en carbone, **et matière fraiche** (MF, aussi appellée matière verte ou déchets humides), riche en azote. Il faut alterner, mélanger, car les MF se décomposent plus rapidement. La MS structure le compost, lui donne ses propriétés physiques, tandis que la MF le nourrit et lui apporte des nutriments, sels minéraux, etc. Exemples de MF : épluchures, fruits et légumes, fleurs, feuilles vertes, marc de café. Exemples de MS : paille, tiges dures, branches d’arbres, feuilles mortes, papier, carton, sciure, écorces, cendre.

Commencer un compost : commencer par une couche de déchets grossiers (branches, etc) pour aérer et attirer les micro-organismes. Puis une couche de compost mûr (ou de terre par défaut) pour stimuler la décomposition. Puis on peut commencer à composter, en alternant MS et MF, et en humidifiant et en aérant selon les besoins.

Un compost humide n’est pas bon car il se tasse et pourrit, un compost trop sec n’est pas bon non plus car les animaux à l’intérieur ont besoin d’eau. Une poignée de compost prise au coeur du tas ne doit ni goutter ou sentir mauvais (trop humide), ni se désagréger (trop sec). D’autres indices permettent également de se rendre compte de l’humidité du compost. Il ne faut pas hésiter à l’arroser s’il est trop sec, ou à l’étaler au soleil temporairement s’il est trop humide, et ajouter de la MF ou de la MS selon. Il faut également le brasser régulièrement, sur 30-40cm de profondeur, toutes les semaines pour un compost en silo et tous les 6 mois pour un compost en tas. Dans tous les cas, une surveillance régulière de son humidité et de son aération est nécessaire.

Un compost est mûr lorsqu’il dégage une odeur d’humus forestier, qu’il est régulier et agréable au toucher, et que des animaux sont dedans. Il n’est pas mûr s’il dégage une odeur d’ammoniaque ou s’il est visqueux.

Le livre comporte un dernier chapitre sur le lombricompstage, mais comme je n’ai pas compris grand-chose, je vais m’abstenir d’en faire un résumé. Il faudrait que j’en voie ou que j’en utilise un pour mieux expliquer/décrire/résumer la chose.
