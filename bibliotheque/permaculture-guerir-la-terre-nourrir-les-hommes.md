---
description: 'Auteurs : Perrine et Charles Hervé-Gruyer, 2014, 410 pages'
---

# Permaculture - guérir la terre, nourrir les hommes

{% file src="../.gitbook/assets/Charles et Perrine Hervé-Gruyer - Permaculture - Guerir la terre nourrir les hommes.pdf" %}
