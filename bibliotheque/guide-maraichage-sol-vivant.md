---
description: >-
  Réalisé et édité par l'association Maraîchage Sol Vivant Normandie, édition
  2022, 243 pages
---

# Guide Maraîchage Sol Vivant

### Présentation :

Ce guide aborde beaucoup de points différents et est très complet sous beaucoup d'aspects, il serait compliqué d'en faire un résumé court et pertinent. Deux points intéressants cependants, il y a des fiches de culture pour plus de 20 plantes et une vingtaine de fermes en MSV sont présentées en détail à la fin du livre. Il y a également une bibliographie et une vidéographie.

### Sommaire :&#x20;

Introduction 3

Avertissement 10&#x20;

Le cycle de la fertilité et le bilan humique 15&#x20;

Les vers de terre dans l’écosystème sol 21&#x20;

Diagnostic de son sol 24&#x20;

Stratégie de gestion de la fertilité 33&#x20;

Réaliser son bilan humique 42&#x20;

Vers le zéro désherbage ? 44&#x20;

Gestion des maladies et des ravageurs 50&#x20;

Conditionnement et conservation des légumes 58&#x20;

Commercialisation et transformation 62&#x20;

Produire ses propres semences 68&#x20;

L’installation en MSV 72&#x20;

Conversion en MSV 84&#x20;

Jardin amateur 85&#x20;

Agroforesterie : l’arbre au cœur du système maraîcher 86&#x20;

Avoir un atelier poules pondeuses 92&#x20;

Introduction aux itinéraires techniques 97&#x20;

Conseils "si c'était à refaire" 100

Fiches plantes 103

Portraits de fermes 155

Bibliograhie et vidéographie 242
