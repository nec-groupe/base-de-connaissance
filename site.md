---
description: >-
  Description du site de Mauves, importance de trouver le bon site, et manières
  d'évaluer un site donné pour cultiver
---

# 🏝 Site

### Mauves-sur-Loire :&#x20;

(présentation)

### En général :&#x20;

Critères importants : fertilité du sol (démarrer avec le meilleur sol possible est un choix judicieux), climat (le climat de la région détermine le nombre de jours sans gel, la température moyenne etc, et donc le potentiel de production), orientation (passage du soleil, ensoleillement des terres, ombres portées, etc), infrastructures, possibilité de transporter les légumes vers leur lieu de consommation, vents dominants, végétation déjà présente,  etc.&#x20;

D'autres critères, comme le prix, la beauté, ou le contexte politique au sens large, comptent aussi mais ne doivent pas forcément être la priorité.

Il ne faut pas hésiter à s'inspirer de l'ingéniosité d'autres maraîchers pour l'organisation du site, les aménagements, les infrastructures, la circulation, etc. Il peut aussi être une bonne idée de visiter d'autres fermes maraîchères, indépendamment de leur type ou de leur taille.&#x20;

#### Superficie

Une terre trop grande est un fardeau financier, technique et de travail pour une production soignée et manuelle.

#### Topographie

L'idéal est un terrain en pente faible et constante, sans creux, orientée vers le sud pour profiter d'un maximum d'ensoleillement. On peut ainsi contrer les crues et les pluies torrentielles, avec par exemple des planches surélevées (et donc drainantes) et une pente douce et des rigoles qui favorisent l'égouttement de surface. Il faut éviter les formes de cuvette et les orientations au nord pour qu'un maximum de lumière arrive sur le terrain et pour que la terre se réchauffe plus rapidement. La pente permet également à l'air de circuler, et permet d'éviter certaines maladies. Il faut éviter la localisation en bas d'une pente ou au creux d'une vallée, car le terrain subit les premiers gels à cause de la convection de l'air (l'air froid à tendance à descendre).&#x20;

Il peut être pertinent d'arpenter le terrain lors d'une pluie abondante pour voir comment l'eau circule, et retourner dessus plus tard pour voir où l'eau stagne toujours.

#### Infrastructures

Il faut un bâtiment pour le rangement du matériel, éventuellement pour le stockage ou le conditionnement des légumes, avec de l'eau potable, de l'électricité, une bonne isolation, une voie d'accès aux véhicules, et tout ça pas trop loin du champ. Il ne faut pas hésiter à faire un abri temporaire pour évaluer les besoins d'un abri définitif. En tous les cas, il faut prioriser la praticité.&#x20;

#### Pollutions

Il faut faire attention à la pollution agricole et industrielle autour du champ. Par exemple, certains insecticides comme l'arséniate de plomb étaient utilisés dans les vergers jusque dans les années 70. La monoculture conventionnelle rend les sols très pauvres et peu adaptés à la culture, et l'épandage de produits juste à côté d'un champ en bio peut détruire des récoltes. Dans l'idéal, il faut faire l'historique du site et analyser les sols.&#x20;

#### Aménagement du site

Il est important d'aménager intelligemment le site pour favoriser le travail efficace dans les opérations quotidiennes. Il faut avoir une vue d'ensemble de tous les éléments immobiliers et former un plan bien fini et bien solide. Il faut planifier la circulation et mettre les endroits les plus fréquentés proches des jardins (lavage, toilettes, outils, stockage, etc). L'idéal est de centrer ces choses au centre du jardin, sous un même toit, puis de planifier les espaces intérieurs.&#x20;
